@extends('template')

@section('main')
	<div id="produktoko" class="panel panel-default">
		<div class="panel-heading"><b><h4>Tambah Produk Toko</h4></b></div>
		<div class="panel-body">
		{!! Form::open(['url' => 'produktoko', 'files' => true]) !!}

		@include('produktoko.form', ['submitButtonText' => 'Tambah Produk Toko'])

		{!! Form::close() !!}
		</div>
	</div>
@stop

@section('footer')
	@include('footer')
@stop