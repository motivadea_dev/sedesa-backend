@if (isset($subkategoriproduk))
{!! Form::hidden('id', $subkategoriproduk->id) !!}
@endif

{{--  Kategori --}}
<div class="form-group">
	{!! Form::label('id_kategoriproduk','Kategori Produk',['class' => 'control-label']) !!}
	@if(count($daftarkategoriproduk) > 0)
	{!! Form::select('id_kategoriproduk', $daftarkategoriproduk, null,['class' => 'form-control', 'id'=>'id_kategoriproduk','placeholder'=>'Pilih Kategori Produk']) !!}
	@else
	<p>Tidak ada pilihan Kategori,silahkan buat dulu.</p>
	@endif
	@if ($errors->has('id_kategoriproduk'))
	<span class="help-block">{{ $errors->first('id_kategoriproduk') }}</span>
	@endif
</div>

{{-- Sub Kategori --}}
@if($errors->any())
<div class="form-group {{ $errors->has('namasubkategori') ? 'has-error' : 'has-success' }}"></div>
@else
<div class="form-group">
@endif
	{!! Form::label('namasubkategori','Sub Kategori Produk',['class' => 'control-label']) !!}
	{!! Form::text('namasubkategori', null,['class' => 'form-control']) !!}
	@if ($errors->has('namasubkategori'))
	<span class="help-block">{{ $errors->first('namasubkategori') }}</span>
	@endif
</div>


{{-- Submit button --}}
<div class="form-group">
	{!! Form::submit($submitButtonText,['class' => 'btn btn-primary form-control']) !!}
</div>