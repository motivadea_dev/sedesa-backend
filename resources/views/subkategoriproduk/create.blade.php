@extends('template')

@section('main')
	<div id="subkategoriproduk" class="panel panel-default">
		<div class="panel-heading"><b><h4>Tambah Sub Kategori Produk</h4></b></div>
		<div class="panel-body">
		{!! Form::open(['url' => 'subkategoriproduk', 'files' => true]) !!}

		@include('subkategoriproduk.form', ['submitButtonText' => 'Tambah Sub Kategori Produk'])

		{!! Form::close() !!}
		</div>
	</div>
@stop

@section('footer')
	@include('footer')
@stop