@extends('template')

@section('main')
	<div id="posting" class="panel panel-default">
		<div class="panel-heading"><b><h4>Ubah Posting</h4></b></div>
		<div class="panel-body">
		{!! Form::model($posting, ['method' => 'PATCH', 'action' => ['PostingwebController@update', $posting->id],'files'=>true]) !!}

		@include('posting.form', ['submitButtonText' => 'Update Posting'])
		
		{!! Form::close() !!}
		</div>
	</div>
@stop

@section('footer')
	@include('footer')
@stop