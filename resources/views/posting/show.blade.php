@extends('template')
@section('main')
<div id="posting" class="panel panel-default">
	<div class="panel-heading"><b><h4>Nama Posting</h4></b></div>
@if (isset($posting))
	{!! Form::model($posting, ['method' => 'PATCH', 'action' => ['PostingwebController@verifikasi', $posting->id],'files'=>true]) !!}
	{!! Form::hidden('id', $posting->id) !!}
	<div class="panel-body">
	{{-- Status --}}
	<div class="form-group">
	{!! Form::label('status','Aktif',['class' => 'control-label']) !!}
		<div class="radio">
		<label>
		{!! Form::radio('status','2') !!} Ya
		</label>
		</div>
		<div class="radio">
		<label>
		{!! Form::radio('status','1') !!} Tidak
		</label>
		</div>
	</div>
	
	@if($posting->status == 2)
	{{-- Headline --}}
	<div class="form-group">
	{!! Form::label('headline','Headline',['class' => 'control-label']) !!}
		<div class="radio">
		<label>
		{!! Form::radio('headline','2') !!} Ya
		</label>
		</div>
		<div class="radio">
		<label>
		{!! Form::radio('headline','1') !!} Tidak
		</label>
		</div>
	</div>
	@endif

	{{-- Tombol --}}
	<div class="row">
		<div class="col-md-3">
		<div class="form-group">
		{!! Form::submit('Simpan',['class' => 'btn btn-success form-control']) !!}
		</div>
		</div>
	</div>
	{!! Form::close() !!}
@endif

		<table class="table table-striped">
		<tr><th>Kategori Posting</th><td>{{ $posting->kategoripost->namakategori }}
		</td></tr>
		<tr><th>Nama Warga</th><td>{{ $posting->warga->nama }}
		</td></tr>
		<tr><th>Tanggal</th><td>{{ $posting->tanggal }}
		</td></tr>
		<tr><th>Judul</th><td>{{ $posting->judul }}
		</td></tr>
		<tr><th>Isi Posting</th><td>{!! $posting->isipost !!}</td></tr>
		<tr><th>Foto</th><td><img width="200" height="200" src="{{ asset('fotoupload/' . $posting->foto) }}">
		<tr><th>Tags</th><td>{{ $posting->tag }}
		</td></tr>
		</table>
	</div>
	</div>
</div>
@stop

@section('footer')
@include('footer')
@stop