@if (isset($posting))
{!! Form::hidden('id', $posting->id) !!}
@endif

{{--  Kategori Posting --}}
<div class="form-group">
	{!! Form::label('id_kategoripost','Kategori Posting',['class' => 'control-label']) !!}
	@if(count($daftarkategoripost) > 0)
	{!! Form::select('id_kategoripost', $daftarkategoripost, null,['class' => 'form-control js-example-basic-single', 'id'=>'id_kategoripost','placeholder'=>'Pilih Kategori Posting']) !!}
	@else
	<p>Tidak ada pilihan Kategori Posting,silahkan buat dulu.</p>
	@endif
	@if ($errors->has('id_kategoripost'))
	<span class="help-block">{{ $errors->first('id_kategoripost') }}</span>
	@endif
</div>

{{--  Nama Warga--}}
<div class="form-group">
	{!! Form::label('id_warga','Nama Warga',['class' => 'control-label']) !!}
	@if(count($daftarwarga) > 0)
	{!! Form::select('id_warga', $daftarwarga, null,['class' => 'form-control js-example-basic-single', 'id'=>'id_warga','placeholder'=>'Pilih Nama Warga']) !!}
	@else
	<p>Tidak ada Nama Warga,silahkan buat dulu.</p>
	@endif
	@if ($errors->has('id_warga'))
	<span class="help-block">{{ $errors->first('id_warga') }}</span>
	@endif
</div>

{{-- Tanggal --}}
@if($errors->any())
<div class="form-group {{ $errors->has('tanggal') ? 'has-error' : 'has-success' }}"></div>
@else
<div class="form-group">
@endif
	{!! Form::label('tanggal','Tanggal',['class' => 'control-label']) !!}
	{!! Form::date('tanggal', null,['class' => 'form-control']) !!}
	@if ($errors->has('tanggal'))
	<span class="help-block">{{ $errors->first('tanggal') }}</span>
	@endif
</div>

{{-- Judul --}}
@if($errors->any())
<div class="form-group {{ $errors->has('judul') ? 'has-error' : 'has-success' }}"></div>
@else
<div class="form-group">
@endif
	{!! Form::label('judul','Judul Posting',['class' => 'control-label']) !!}
	{!! Form::text('judul', null,['class' => 'form-control']) !!}
	@if ($errors->has('judul'))
	<span class="help-block">{{ $errors->first('judul') }}</span>
	@endif
</div>

{{-- Isi Posting --}}
@if($errors->any())
<div class="form-group {{ $errors->has('isipost') ? 'has-error' : 'has-success' }}"></div>
@else
<div class="form-group">
@endif
	{!! Form::label('isipost','Isi Posting',['class' => 'control-label']) !!}
	{!! Form::textarea('isipost', null,['class' => 'form-control','id' => 'isipost']) !!}
	@if ($errors->has('isipost'))
	<span class="help-block">{{ $errors->first('isipost') }}</span>
	@endif
</div>

{{-- Foto --}}
@if($errors->any())
<div class="form-group {{ $errors->has('foto') ? 'has-error' : 'has-success' }}"></div>
@else
<div class="form-group">
@endif
	{!! Form::label('foto','Foto') !!}
	{!! Form::file('foto') !!}
	@if ($errors->has('foto'))
	<span class="help-block">{{ $errors->first('foto') }}</span>
	@endif
</div>

{{-- Tags --}}
<div class="form-group">
	{!! Form::label('tag','Tags',['class' => 'control-label']) !!}
	{!! Form::text('tag', null,['class' => 'form-control']) !!}
</div>

{{-- Status --}}
@if($errors->any())
<div class="form-group {{ $errors->has('status') ? 'has-error' : 'has-success' }}"></div>
@else
<div class="form-group">
@endif
{!! Form::label('status','Aktif',['class' => 'control-label']) !!}
	<div class="radio">
	<label>
	{!! Form::radio('status','2') !!} Ya
	</label>
	</div>
	<div class="radio">
	<label>{!! Form::radio('status','1') !!} Tidak
	</label>
	</div>
	@if ($errors->has('status'))
	<span class="help-block">{{ $errors->first('status') }}</span>
	@endif
</div>

{{-- Headline --}}
@if($errors->any())
<div class="form-group {{ $errors->has('headline') ? 'has-error' : 'has-success' }}"></div>
@else
<div class="form-group">
@endif
{!! Form::label('headline','Headline',['class' => 'control-label']) !!}
	<div class="radio">
	<label>
	{!! Form::radio('headline','2') !!} Ya
	</label>
	</div>
	<div class="radio">
	<label>{!! Form::radio('headline','1') !!} Tidak
	</label>
	</div>
	@if ($errors->has('headline'))
	<span class="help-block">{{ $errors->first('headline') }}</span>
	@endif
</div>

{{-- Submit button --}}
<div class="form-group">
	{!! Form::submit($submitButtonText,['class' => 'btn btn-primary form-control']) !!}
</div>

<script type="text/javascript">
$(document).ready(function(){
	$('.js-example-basic-single').select2();
});
  tinymce.init({
	branding: false,
    selector: '#isipost',
    theme: 'modern',
    plugins: [
      'advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker',
      'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
      'save table contextmenu directionality emoticons template paste textcolor'
    ],
    content_css: 'css/content.css',
    toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons'
  });
</script>