@extends('template')
@section('main')
<div id="homepage" class="panel panel-default">
	<div class="panel-heading"><b><h4>Selamat Datang, {{ Auth::user()->name }}</h4></b></div>
	<div class="panel-body" align="center"><h3>SISTEM INFORMASI DESA TERPADU <br>SEDESA.id</h3></div>
</div>
@stop

@section('footer')
	@include('footer')
@stop