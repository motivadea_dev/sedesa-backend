@if (isset($posting))
{!! Form::hidden('id', $posting->id) !!}
@endif
<div class="row">
<!-- KIRI -->
<div class="col-md-7">
{{-- Judul --}}
@if($errors->any())
<div class="form-group {{ $errors->has('judul') ? 'has-error' : 'has-success' }}"></div>
@else
<div class="form-group">
@endif
	{!! Form::label('judul','Judul Posting',['class' => 'control-label']) !!}
	{!! Form::text('judul', null,['class' => 'form-control','id' => 'judul']) !!}
	@if ($errors->has('judul'))
	<span class="help-block">{{ $errors->first('judul') }}</span>
	@endif
</div>

{{-- Isi Posting --}}
@if($errors->any())
<div class="form-group {{ $errors->has('isipost') ? 'has-error' : 'has-success' }}"></div>
@else
<div class="form-group">
@endif
	{!! Form::label('isipost','Isi Posting',['class' => 'control-label']) !!}
	{!! Form::textarea('isipost', null,['class' => 'form-control','id' => 'isipost']) !!}
	@if ($errors->has('isipost'))
	<span class="help-block">{{ $errors->first('isipost') }}</span>
	@endif
</div>

<!-- AKHIR KIRI -->
</div>

<!-- KANAN -->
<div class="col-md-5">
{{-- Judul SEO --}}
<div class="form-group">
	{!! Form::label('judulseo','Judul SEO',['class' => 'control-label']) !!}
	{!! Form::text('judulseo', null,['class' => 'form-control','id' => 'judulseo','readonly']) !!}
</div>

{{--  Kategori Posting --}}
<div class="form-group">
	{!! Form::label('id_kategoripost','Kategori Posting',['class' => 'control-label']) !!}
	@if(count($daftarkategoripost) > 0)
	{!! Form::select('id_kategoripost', $daftarkategoripost, null,['class' => 'form-control js-example-basic-single', 'id'=>'id_kategoripost','placeholder'=>'Pilih Kategori Posting']) !!}
	@else
	<p>Tidak ada pilihan Kategori Posting,silahkan buat dulu.</p>
	@endif
	@if ($errors->has('id_kategoripost'))
	<span class="help-block">{{ $errors->first('id_kategoripost') }}</span>
	@endif
</div>

{{-- Tags --}}
<div class="form-group">
	{!! Form::label('tag','Tags',['class' => 'control-label']) !!}
	{!! Form::text('tag', null,['class' => 'form-control tag','id' => 'tag']) !!}
</div>

{{-- Foto --}}
@if($errors->any())
<div class="form-group {{ $errors->has('foto') ? 'has-error' : 'has-success' }}"></div>
@else
<div class="form-group">
@endif
	{!! Form::label('foto','Gambar') !!}
	{!! Form::file('foto') !!}
	@if ($errors->has('foto'))
	<span class="help-block">{{ $errors->first('foto') }}</span>
	@endif
</div>

{{-- Tanggal --}}
@if($errors->any())
<div class="form-group {{ $errors->has('tanggal') ? 'has-error' : 'has-success' }}"></div>
@else
<div class="form-group">
@endif
	{!! Form::label('tanggal','Tanggal',['class' => 'control-label']) !!}
	{!! Form::date('tanggal', null,['class' => 'form-control']) !!}
	@if ($errors->has('tanggal'))
	<span class="help-block">{{ $errors->first('tanggal') }}</span>
	@endif
</div>

<!-- AKHIR KANAN -->
</div>
</div>

{!! Form::hidden('id_users', Auth::user()->id) !!}

<div class="row">
<!-- KIRI -->
<div class="col-md-4">
{{-- Submit button --}}
<div class="form-group">
	{!! Form::submit($submitButtonText,['class' => 'btn btn-primary form-control']) !!}
</div>

<!-- AKHIR KIRI -->
</div>

<!-- KANAN -->
<div class="col-md-8">

<!-- AKHIR KANAN -->
</div>
</div>

<script type="text/javascript">
$(document).ready(function(){
	$('.js-example-basic-single').select2();
	$('.tag').tagsInput();
	$('#judul').on('input', function() {
		var permalink;
		// Trim empty space
		permalink = $.trim($(this).val());
	
		// replace more then 1 space with only one
		permalink = permalink.replace(/\s+/g,' ');

		$('#judulseo').val(permalink.toLowerCase());
		$('#judulseo').val($('#judulseo').val().replace(/\W/g, ' '));
		$('#judulseo').val($.trim($('#judulseo').val()));
		$('#judulseo').val($('#judulseo').val().replace(/\s+/g, '-'));
	});
});
  tinymce.init({
	branding: false,
    selector: '#isipost',
    theme: 'modern',
    plugins: [
      'advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker',
      'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
      'save table contextmenu directionality emoticons template paste textcolor'
    ],
    content_css: 'css/content.css',
    toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons'
  });
</script>