@extends('template')

@section('main')
<div id="posting" class="panel panel-default">
	<div class="panel-heading"><b><h4>Posting</h4></b></div>
	<div class="panel-body">
	@include('_partial.flash_message')
	<div class="tombol-nav">
		{{ link_to('userposting/create','Tambah Posting',['class' => 'btn btn-primary']) }}
	</div><br><br><br>
	@if (count($daftarposting) > 0)
	<table class="table">
		<thead>
			<tr>
			<!-- Tampil fild -->
				<th>ID</th>
				<th>Judul</th>
				<th>Kategori</th>
				<th>Active</th>
				<th>Headline</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
			<?php $i=0; ?>
			<?php foreach($daftarposting as $posting): ?>
			<tr>
				<!-- Tampil Data (Perulangan) -->
				<td>{{ $posting->id }}</td>
				<td>{{ $posting->judul }}</td>
				<td>{{ $posting->kategoripost->namakategori }}</td>
				@if($posting->status == 1)
				<td><span class="glyphicon glyphicon-remove" style="color:red;"></span> Tidak </td>
				@elseif($posting->status == 2)
				<td><span class="glyphicon glyphicon-ok" style="color:green;"></span> Ya</td>
				@endif
				
				@if($posting->headline == 1)
				<td><span class="glyphicon glyphicon-remove" style="color:red;"></span> Tidak </td>
				@elseif($posting->headline == 2)
				<td><span class="glyphicon glyphicon-ok" style="color:green;"></span> Ya</td>
				@endif

				<!-- Tombol -->
				<td>
					<div class="box-button"> 
					{{ link_to('detailinformasi/' . $posting->judulseo,'Preview',['class' => 'btn btn-info btn-sm','target' => '_blank']) }}</div>
					<div class="box-button"> 
					{{ link_to('userposting/komentar/' . $posting->id,'Komentar',['class' => 'btn btn-primary btn-sm']) }}</div>
					<!--<div class="box-button"> 
					{{ link_to('posting/polling/' . $posting->id,'Polling',['class' => 'btn btn-default btn-sm']) }}</div>-->
					<div class="box-button">
					{{ link_to('userposting/' . $posting->id,'&nbsp;&nbsp;Detail&nbsp;',['class' => 'btn btn-success btn-sm']) }}
					</div> 
					<div class="box-button">
					{{ link_to('userposting/' . $posting->id . '/edit', 'Edit', ['class' => 'btn btn-warning btn-sm']) }}
					</div>
					<div class="box-button">
					{!! Form::open(['method' => 'DELETE', 'action' => ['UserPostingwebController@destroy',$posting->id]]) !!}
					{!! Form::submit('Hapus', ['class' => 'btn btn-danger btn-sm'])!!}
					{!! Form::close()!!}
					</div>
				</td>
			</tr>
		<?php endforeach ?>
		</tbody>
	</table>
	@else
	<p>Tidak Ada Posting</p>
	@endif
	<div class="table-nav">
	<div class="jumlah-data">
		<strong>Jumlah Posting : {{ $jumlahposting }}</strong>
	</div>
	<div class="paging">
	{{ $daftarposting->links() }}
	</div>
	</div>

	</div>
</div>
@stop

@section('footer')
	@include('footer')
@stop