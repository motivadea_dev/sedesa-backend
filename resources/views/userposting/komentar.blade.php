@extends('template')

@section('main')
<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog" style="margin-top:100px;">
<div class="modal-dialog">
	<!-- Modal Produk-->
	<div class="modal-content">
		<div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <div><b>Balas Komentar</b></div>
        </div>
        <div class="modal-body">
			{!! Form::open(['url' => 'userposting/balasan', 'files' => true]) !!}
			{!! Form::hidden('id_komentarpost', null,['class' => 'form-control','id'=>'id_komentarpost']) !!}
			<div class="form-group">
				{!! Form::textarea('komentar', null,['class' => 'form-control']) !!}
			</div>
			{{-- Submit button --}}
			<div class="form-group">
				{!! Form::submit('Simpan',['class' => 'btn btn-primary form-control']) !!}
			</div>	
			{!! Form::close() !!}	
		</div>
	</div>
</div>
</div>
<!-- End Modal -->
<div id="komentar" class="panel panel-default">
	<div class="panel-heading"><b><h4>Komentar</h4></b></div>
	<div class="panel-body">
	@include('_partial.flash_message')
	@if (count($daftarkomentar) > 0)
	<table class="table">
		<thead>
			<tr>
			<!-- Tampil fild -->
				<th>Tanggal</th>
				<th>Nama</th>
                <th>Email</th>
				<th>Komentar</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
			<?php $i=0; ?>
			<?php foreach($daftarkomentar as $komentar): ?>
			<tr>
				<!-- Tampil Data (Perulangan) -->
				<td>{{ $komentar->tanggal }}</td>
				<td>{{ $komentar->nama }}</td>
                <td>{{ $komentar->email }}</td>
				<td>{{ $komentar->komentar }}<br><b>Balasan</b><br>
				@foreach($komentar->balaskomentar as $balasan)
				- {{ $balasan->komentar }} 
					{!! Form::open(['method' => 'DELETE', 'action' => ['UserPostingwebController@destroybalasan',$balasan->id]]) !!}
					{!! Form::submit('Hapus', ['class' => 'btn btn-danger btn-sm'])!!}
					{!! Form::close()!!}<br>
				@endforeach
				</td>
				<td>
					<div class="box-button"> 
					{{ link_to('#','Balas',['class' => 'btn btn-warning btn-sm','id'=>'tbbalas'.$komentar->id]) }}</div>
                    <div class="box-button">
					{!! Form::open(['method' => 'DELETE', 'action' => ['UserPostingwebController@destroykomentar',$komentar->id]]) !!}
					{!! Form::submit('Hapus', ['class' => 'btn btn-danger btn-sm'])!!}
					{!! Form::close()!!}
					</div>
				</td>
			</tr>
		<?php endforeach ?>
		</tbody>
	</table>
	@else
	<p>Tidak ada Komentar</p>
	@endif
	<div class="table-nav">
	<div class="jumlah-data">
		<strong>Jumlah Komentar : {{ $jumlahkomentar }}</strong>
	</div>
	</div>

	</div>
</div>
<script>
$(document).ready(function(){
	<?php
	echo "var daftarkomentar ={$daftarkomentar}; ";
	?>
	var i = 0;
    for (i = 0; i < daftarkomentar.length; i++) {
    (function(i) {
        $("#tbbalas" + daftarkomentar[i].id).click(function() {
			document.getElementById("id_komentarpost").value =  daftarkomentar[i].id;
			$("#myModal").modal({backdrop: "static"});
        });
      })(i);
    }
});
</script>
@stop

@section('footer')
	@include('footer')
@stop