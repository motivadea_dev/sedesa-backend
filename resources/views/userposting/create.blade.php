@extends('template')

@section('main')
	<div id="posting" class="panel panel-default">
		<div class="panel-heading"><b><h4>Tambah Posting</h4></b></div>
		<div class="panel-body">
		{!! Form::open(['url' => 'userposting', 'files' => true]) !!}

		@include('userposting.form', ['submitButtonText' => 'Tambah Posting'])

		{!! Form::close() !!}
		</div>
	</div>
@stop

@section('footer')
	@include('footer')
@stop