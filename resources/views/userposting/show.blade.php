@extends('template')
@section('main')
<div id="posting" class="panel panel-default">
	<div class="panel-heading"><b><h4>Detail Posting</h4></b></div>
		<table class="table table-striped">
		<tr><th>Kategori Posting</th><td>{{ $posting->kategoripost->namakategori }}
		</td></tr>
		<tr><th>Nama Warga</th><td>{{ $posting->user->name }}
		</td></tr>
		<tr><th>Tanggal</th><td>{{ $posting->tanggal }}
		</td></tr>
		<tr><th>Judul</th><td>{{ $posting->judul }}
		</td></tr>
		<tr><th>Isi Posting</th><td>{!! $posting->isipost !!}</td></tr>
		<tr><th>Foto</th><td><img width="200" height="200" src="{{ asset('fotoupload/' . $posting->foto) }}">
		<tr><th>Tags</th><td>{{ $posting->tag }}
		</td></tr>
		</table>
	</div>
	</div>
</div>
@stop

@section('footer')
@include('footer')
@stop