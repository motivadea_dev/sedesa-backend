@extends('template')

@section('main')
<div id="produkbumdes" class="panel panel-default">
	<div class="panel-heading"><b><h4>Produk Bumdes</h4></b></div>
	<div class="panel-body">
	@include('_partial.flash_message')
	<div class="tombol-nav">
		{{ link_to('produkbumdes/create','Tambah Produk',['class' => 'btn btn-primary']) }}
	</div><br><br><br>
	@if (count($daftarprodukbumdes) > 0)
	<table class="table">
		<thead>
			<tr>
			<!-- Tampil fild -->
				<th>Kode Produk</th>
				<th>Nama Produk</th>
				<th>Stok</th>
				<th>Harga</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
			<?php $i=0; ?>
			<?php foreach($daftarprodukbumdes as $produkbumdes): ?>
			<tr>
				<!-- Tampil Data (Perulangan) -->
				<td>{{ $produkbumdes->kodeproduk }}</td>
				<td>{{ $produkbumdes->namaproduk }}</td>
				<td>{{ $produkbumdes->stok }}</td>
				<td>{{ $produkbumdes->harga }}</td>
				<!-- Tombol -->
				<td>
					<div class="box-button">
					{{ link_to('produkbumdes/' . $produkbumdes->id,'Detail',['class' => 'btn btn-success btn-sm']) }}</div> 
					<div class="box-button">
					{{ link_to('produkbumdes/' . $produkbumdes->id . '/edit', 'Edit', ['class' => 'btn btn-warning btn-sm']) }}
					</div>
					<div class="box-button">
					{!! Form::open(['method' => 'DELETE', 'action' => ['ProdukbumdeswebController@destroy',$produkbumdes->id]]) !!}
					{!! Form::submit('Delete', ['class' => 'btn btn-danger btn-sm'])!!}
					{!! Form::close()!!}
					</div>
					<div class="box-button"> 
					{{ link_to('produkbumdes/print/' . $produkbumdes->id,'Cetak',['class' => 'btn btn-primary btn-sm','target'=>'_blank']) }}</div>
				</td>
			</tr>
		<?php endforeach ?>
		</tbody>
	</table>
	@else
	<p>Tidak ada Nama Produk</p>
	@endif
	<div class="table-nav">
	<div class="jumlah-data">
		<strong>Jumlah Toko : {{ $jumlahprodukbumdes }}</strong>
	</div>
	<div class="paging">
	{{ $daftarprodukbumdes->links() }}
	</div>
	</div>

	</div>
</div>
@stop

@section('footer')
	@include('footer')
@stop