@extends('cms.index')
@section('main')
<header class="inner"> 
  <!-- Banner -->
  <div class="header-content">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <h1 id="homeHeading"><a href="{{ url('home') }}">Home</a> / Informasi</h1>
        </div>
      </div>
    </div>
  </div>
</header>

<!-- blogs  -->
<section class="section-bottom-border">
  <div class="container">
    <div class="row">
      @if (count($daftarterbaru) > 0)
      <div class="col-md-8 list-container"> 
        @foreach($daftarterbaru as $terbaru)
        <!--Post -->
        <div class="post-preview"> <a href="{{ url('detailinformasi/'. $terbaru->judulseo) }}">
          <div class="list-thumb" style="background-image: url({{ asset('fotoupload/' . $terbaru->foto) }});">
            <div></div>
          </div>
          <h2 class="post-title"><b>{{ $terbaru->judul }}</b></h2>
          </a>
          <p class="post-meta"><i class="glyphicon glyphicon-user"></i> <a href="#">{{$terbaru->user->name}}</a> &nbsp;&nbsp;<i class="glyphicon glyphicon-calendar"></i> {{ $terbaru->created_at->format('d-m-Y H:i') }} &nbsp;&nbsp;<i class="glyphicon glyphicon-comment"></i> 
          {{ count($terbaru->komentarpost) }} Komentar
          </p>
        </div>
        <hr>
        @endforeach

        <!-- Pager -->
        <div class="table-nav">
          <div class="jumlah-data">
            <strong>Total Informasi : {{ $jumlahterbaru }}</strong>
          </div>
          <div class="paging">
          {{ $daftarterbaru->links() }}
          </div>
        </div>
      </div>
      @else
      <div class="col-md-8 list-container"> 
        <h3>Maaf,Informasi untuk kategori ini belum ada.</h3>
      </div>
      @endif
      <!-- ==== Sidebar Starts Here ==== -->
      <div class="col-md-4 sidebar"> 
         <!--Sidebar Popular Posts-->
        <h2>Informasi Populer</h2>
        @foreach($daftarpopuler as $populer)
        <div class="sidebar-post"> <a href="{{ url('detailinformasi/'. $populer->judulseo) }}">{{ $populer->judul }}</a>
          <p class="post-meta">&nbsp;&nbsp;&nbsp;&nbsp;Diposting Oleh <a href="#">{{$populer->user->name}}</a> {{$populer->created_at->format('d-m-Y H:i')}}</p>
        </div>
        @endforeach
        <!--Sidebar Call to Action
        <div class="sidebar-cta">
          <div> <img src="img/e-book.png" alt="e book"/>
            <h3>Please Download E-book</h3>
            <button class="btn-download">Download</button>
          </div>
        </div>
        <hr>-->

        <!--Sidebar Categories-->
        <h2>Kategori</h2>
        <ul class="sidebar-list">
          @foreach($daftarkategoripost as $kategoripost)
            <li><a href="{{ url('informasi/'. $kategoripost->id) }}">{{ $kategoripost->namakategori }}</a></li>
          @endforeach
        </ul>
        <hr>
        <!--Sidebar Popular Tags-->
        <h2>Tags Populer</h2>
        <div class="sidebar-tags"> 
        <a href="{{ url('tags/desa') }}">Desa</a> <a href="{{ url('tags/bumdes') }}">Bumdes</a><a href="{{ url('tags/bali') }}">Bali</a>
        <a href="{{ url('tags/pembangunan') }}">Pembagunan</a><a href="{{ url('tags/danadesa') }}">Dana Desa</a><a href="{{ url('tags/jokowi') }}">Jokowi</a>
        <a href="{{ url('tags/dusun') }}">Dusun</a><a href="{{ url('tags/banjar') }}">Banjar</a>
        </div>
      <!-- ==== Sidebar Ends Here ==== --> 
    </div>
  </div>
</section>


@stop