@extends('cms.index')
@section('main')
<header class="inner"> 
  <!-- Banner -->
  <div class="header-content">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <h1 id="homeHeading"><a href="index-2.html">Home</a> / Detail Pengaduan</h1>
        </div>
      </div>
    </div>
  </div>
</header>
<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog" style="margin-top:100px;">
<div class="modal-dialog">
	<!-- Modal Produk-->
  <div class="modal-content">
		<div class="modal-header">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
      <div><b>Balas Komentar</b></div>
    </div>
    <div class="modal-body">
			{!! Form::open(['url' => 'balaspengaduan', 'files' => true, 'class' => 'form-horizontal']) !!}
			{!! Form::hidden('id_pengaduan', $pengaduan->id) !!}
      {!! Form::hidden('id_komentarpengaduan', null,['class' => 'form-control','id'=>'id_komentarpengaduan']) !!}
      &nbsp;
      <div class="form-group">
        {!! Form::label('nama','Nama',['class' => 'col-sm-2 control-label']) !!}
        <div class="col-sm-9">
        @if(Auth::check())
        {!! Form::text('nama', Auth::user()->name,['class' => 'form-control','readonly']) !!}
        @else
        {!! Form::text('nama', null,['class' => 'form-control']) !!}
        @endif
        </div>
			</div>
      <div class="form-group">
        {!! Form::label('email','Email',['class' => 'col-sm-2 control-label']) !!}
        <div class="col-sm-9">
        @if(Auth::check())
        {!! Form::text('email', Auth::user()->email,['class' => 'form-control','readonly']) !!}
        @else
        {!! Form::text('email', null,['class' => 'form-control']) !!}
        @endif
        </div>
			</div>
			<div class="form-group">
        {!! Form::label('komentar','Komentar',['class' => 'col-sm-2 control-label']) !!}
        <div class="col-sm-9">
        {!! Form::textarea('komentar', null,['class' => 'form-control']) !!}
        </div>
			</div>
			{{-- Submit button --}}
			<div class="form-group">
        <div class="col-sm-2">
        </div>
        <div class="col-sm-3">
				{!! Form::submit('Simpan',['class' => 'btn btn-primary form-control']) !!}
        </div>
			</div>	
			{!! Form::close() !!}	
		</div>
	</div>

</div>
</div>
<!-- End Modal -->

<!-- Blog single page -->
<section class="section-bottom-border">
  <div class="container">
    <div class="row">
      <div class="col-md-8 list-container post">
        @include('_partial.flash_message')
        <h3><b>{{ $pengaduan->judul }}</b></h3> 
        <p align="left"><i class="glyphicon glyphicon-user"></i> <a href="#">{{$pengaduan->warga->nama}}</a> &nbsp;&nbsp;<i class="glyphicon glyphicon-calendar"></i> {{ $pengaduan->created_at->format('d-m-Y H:i') }}
        @foreach($fotopengaduan as $foto)
        <p align='justify'><img width="100%" src="{{ asset('fotoupload/' . $foto->foto) }}"></p>
        @endforeach
        <p align='justify'>{{ $pengaduan->deskripsi }}</p>
        <div style="width: 100%; height: 400px;">
			  {!! Mapper::render() !!}
        </div>
        <br>
        <div class="post-social-share"> <span>Bagikan ke</span> 
        <?php 
        $url = "http://sedesa.id/detailpengaduan/{{ $pengaduan->id }}";
        ?>
        <a href="https://www.facebook.com/sharer/sharer.php?u={{ urlencode($url) }}" target="_blank"><i class="fa fa-facebook fa-2x" aria-hidden="true"></i></a> 
        <a href="https://twitter.com/intent/tweet?url={{ urlencode($url) }}" target="_blank"><i class="fa fa-twitter fa-2x" aria-hidden="true"></i></a> 
        <a href="https://plus.google.com/share?url={{ urlencode($url) }}" target="_blank"><i class="fa fa-google-plus fa-2x" aria-hidden="true"></i></a> 
        <a href="http://www.linkedin.com/shareArticle?mini=true&url={{ urlencode($url) }}" target="_blank"><i class="fa fa-linkedin fa-2x" aria-hidden="true"></i></a> 
        </div>
        <hr>
        <!-- Pager 
        <ul class="pager">
          <li class="prev"> <a href="javascript:void(0)">&larr; Prev</a> </li>
          <li class="next"> <a href="javascript:void(0)">Next &rarr;</a> </li>
        </ul>-->
        <!-- comments -->
        <h3>Komentar</h3>
        <div class="post-footer">
        {!! Form::open(['url' => 'komentarpengaduan', 'files' => true]) !!}
        {!! Form::hidden('id_pengaduan', $pengaduan->id) !!}
        {{-- Nama --}}
          @if($errors->any())
            <div class="form-group {{ $errors->has('nama') ? 'has-error' : 'has-success' }}">
          @else
            <div class="form-group">
          @endif
              {!! Form::label('nama','Nama',['class' => 'control-label']) !!}
              @if(Auth::check())
              {!! Form::text('nama', Auth::user()->name,['class' => 'form-control','readonly']) !!}
              @else
              {!! Form::text('nama', null,['class' => 'form-control']) !!}
              @endif
          @if ($errors->has('nama'))
            <span class="help-block">{{ $errors->first('nama') }}</span>
          @endif
            </div>

        {{-- Email --}}
          @if($errors->any())
            <div class="form-group {{ $errors->has('email') ? 'has-error' : 'has-success' }}">
          @else
            <div class="form-group">
          @endif
              {!! Form::label('email','Email',['class' => 'control-label']) !!}
              @if(Auth::check())
              {!! Form::text('email', Auth::user()->email,['class' => 'form-control','readonly']) !!}
              @else
              {!! Form::text('email', null,['class' => 'form-control']) !!}
              @endif
          @if ($errors->has('email'))
              <span class="help-block">{{ $errors->first('email') }}</span>
          @endif
            </div>
          
        {{-- Komentar --}}
          @if($errors->any())
            <div class="form-group {{ $errors->has('nama') ? 'has-error' : 'has-success' }}">
          @else
            <div class="form-group">
          @endif
              {!! Form::label('komentar','Komentar',['class' => 'control-label']) !!}
              {!! Form::textarea('komentar', null,['class' => 'form-control']) !!}
          @if ($errors->has('komentar'))
              <span class="help-block">{{ $errors->first('komentar') }}</span>
          @endif
            </div>

        {{-- Submit button --}}
            <div class="">
              {!! Form::submit('Simpan',[ 'buttontype' => 'button','class' => 'btn btn-success' ]) !!}
            </div>
        {!! Form::close() !!} <br>

          <ul class="comments-list">
            @foreach($daftarkomentar as $komentar)
            <!-- KOMENTAR -->
            <li class="comment"> <a class="pull-left" href="#"> <img class="avatar" src="{{ asset('img/user.png') }}" alt="avatar"> </a>
              <div class="comment-body">
                <div class="comment-heading">
                  <h4 class="user"> {{ $komentar->nama }} </h4>
                  <h5 class="time"> {{ $komentar->tanggal }} </h5>
                </div>
                
                <p align="justify"> {{ $komentar->komentar }} </p>
                <div class="box-button"> 
					      {{ link_to('#','Balas',['class' => 'btn btn-primary btn-sm','id'=>'tbbalas'.$komentar->id]) }}</div>
              
                @foreach($komentar->balaskomentarpengaduan as $balasan)
                <!-- Balas Pengaduan -->
                <ul class="comments-list">
                <li class="comment"> <a class="pull-left" href="#"> <img class="avatar" src="{{ asset('img/user.png') }}" alt="avatar"> </a>
                  <div class="comment-body">
                    <div class="comment-heading">
                      <h4 class="user">{{ $balasan->nama }}</h4>
                      <h5 class="time">{{  $balasan->tanggal }}</h5>
                    </div>
                    <p align="justify"> {{  $balasan->komentar }} </p>
                  </div>
                </li>
              </ul>
              <!-- AKHIR BALASAN KOMENTAR -->
              @endforeach
            </li>
            <!-- AKHIR KOMENTAR-->
            @endforeach
          </ul>

        </div>
        <!-- comments end --> 
      </div>
      <!-- ==== Sidebar Starts Here ==== -->
      <div class="col-md-4 sidebar"> 
      <div class="post-social-share"> <span>Bagikan ke</span> 
        <?php 
        $url = "http://sedesa.id/detailpengaduan/{{ $pengaduan->id }}";
        ?>
        <a href="https://www.facebook.com/sharer/sharer.php?u={{ urlencode($url) }}" target="_blank"><i class="fa fa-facebook fa-2x" aria-hidden="true"></i></a> 
        <a href="https://twitter.com/intent/tweet?url={{ urlencode($url) }}" target="_blank"><i class="fa fa-twitter fa-2x" aria-hidden="true"></i></a> 
        <a href="https://plus.google.com/share?url={{ urlencode($url) }}" target="_blank"><i class="fa fa-google-plus fa-2x" aria-hidden="true"></i></a> 
        <a href="http://www.linkedin.com/shareArticle?mini=true&url={{ urlencode($url) }}" target="_blank"><i class="fa fa-linkedin fa-2x" aria-hidden="true"></i></a> 
      </div>
        <!--Sidebar Call to Action
        <div class="sidebar-cta">
          <div> <img src="img/e-book.png" alt="e book"/>
            <h3>Please Download E-book</h3>
            <button class="btn-download">Download</button>
          </div>
        </div>
        <hr>-->
        <!--Sidebar Categories-->
        <h2>Kategori</h2>
        <ul class="sidebar-list">
          @foreach($daftarkategoripengaduan as $kategoripengaduan)
            <li><a href="{{ url('pengaduan/'. $kategoripengaduan->id) }}">{{ $kategoripengaduan->namakategori }}</a></li>
          @endforeach
        </ul>
        <hr>
        <!--Sidebar Popular Tags-->
        </div>
      <!-- ==== Sidebar Ends Here ==== --> 
    </div>
  </div>
</section>
<!-- jQuery --> 
<script src="{{ asset ('cms/assets/jquery/jquery.min.js')}}"></script> 
<script>
$(document).ready(function(){
	<?php
	echo "var daftarkomentar ={$daftarkomentar}; ";
	?>
	var i = 0;
    for (i = 0; i < daftarkomentar.length; i++) {
    (function(i) {
        $("#tbbalas" + daftarkomentar[i].id).click(function() {
			document.getElementById("id_komentarpengaduan").value =  daftarkomentar[i].id;
			$("#myModal").modal({backdrop: "static"});
        });
      })(i);
    }
});
</script>
@stop