<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from konnectplugins.com/edu-course/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 12 Feb 2018 09:59:50 GMT -->
<head>
<!-- Meta -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<meta name="keywords" content="">
<!-- Favicon -->
<link href="{{ asset ('cms/img/fav.png')}}" rel="shortcut icon" type="image/x-icon"/>

<!-- Title -->
<title>Sedesa.id | Portal Informasi Warga Desa Seluruh Indonesia</title>

<!-- Bootstrap Core CSS -->
<link href="{{ asset ('cms/assets/bootstrap/css/bootstrap.css')}}" rel="stylesheet">

<!-- Custom icon Fonts -->
<link href="{{ asset ('cms/assets/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">

<!-- Konnect Slider -->
<link href="{{ asset ('cms/css/konnect-slider.css')}}" media="all" rel="stylesheet" type="text/css" />
<link href="{{ asset ('cms/css/animate.css')}}" media="all" rel="stylesheet" type="text/css" />

<!-- Main CSS -->
<link href="{{ asset ('cms/css/theme.css')}}" rel="stylesheet">
<link href="{{ asset ('cms/css/green.css')}}" rel="stylesheet" id="style_theme">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<!-- Body -->
<body>
<!-- Pre Loader -->
<div class="loading">
  <div class="loader"></div>
</div>
<!-- Scroll to Top --> 
<a id="scroll-up" ><i class="fa fa-angle-up"></i></a> 
<!-- Color Changer 
<div class="theme-settings" id="switcher"> <span class="theme-click"><i class="fa fa-cog fa-spin" aria-hidden="true"></i></span> <span class="theme-color theme-default theme-active" data-color="green"></span> <span class="theme-color theme-blue" data-color="blue"></span> <span class="theme-color theme-red" data-color="red"></span> <span class="theme-color theme-violet" data-color="violet"></span>
  <p>(Or) Your favorite color</p>
</div>-->

@include ('cms.navbar')
@yield ('main')
@include ('cms.footer')

<!-- jQuery --> 
<script src="{{ asset ('cms/assets/jquery/jquery.min.js')}}"></script> 
<!-- Bootstrap Core JavaScript --> 
<script src="{{ asset ('cms/assets/bootstrap/js/bootstrap.min.js')}}"></script> 
<!-- Konnect Slider JavaScript --> 
<script src="{{ asset ('cms/js/jquery.flexslider.min.js')}}" type="text/javascript"></script> 
<script src="{{ asset ('cms/js/konnect-slider.js')}}" type="text/javascript"></script> 
<!-- Theme JavaScript --> 
<script src="{{ asset ('cms/js/default.js')}}"></script>
</body>
</html>