@extends('cms.index')
@section('main')
<header class="inner"> 
  <!-- Banner -->
  <div class="header-content">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <h1 id="homeHeading"><a href="{{ url('home') }}">Home</a> / Pengaduan</h1>
        </div>
      </div>
    </div>
  </div>
</header>

<!-- blogs  -->
<section class="section-bottom-border">
  <div class="container">
    <div class="row">
      @if (count($daftarterbaru) > 0)
      <div class="col-md-8 list-container"> 
        @foreach($daftarterbaru as $terbaru)
        <!--Post -->
        <div class="post-preview"> <a href="{{ url('detailpengaduan/'. $terbaru->id) }}">
          @foreach($terbaru->fotopengaduan as $key => $value)
            @if($key == 0)
            <div class="list-thumb" style="background-image: url({{ asset('fotoupload/' . $value->foto ) }});">
            @endif
          @endforeach
            <div></div>
          </div>
          <h2 class="post-title">{{ $terbaru->judul }}</h2>
          </a>
          <p class="post-meta"><i class="glyphicon glyphicon-user"></i> <a href="#">{{$terbaru->warga->nama}}</a> &nbsp;&nbsp;<i class="glyphicon glyphicon-calendar"></i> {{ $terbaru->created_at->format('d-m-Y H:i') }}
        </div>
        <hr>
        @endforeach

        <!-- Pager -->
        <div class="table-nav">
          <div class="jumlah-data">
            <strong>Total Pengaduan : {{ $jumlahterbaru }}</strong>
          </div>
          <div class="paging">
          {{ $daftarterbaru->links() }}
          </div>
        </div>
      </div>
      @else
      <div class="col-md-8 list-container"> 
        <h3>Maaf,Pengaduan ini belum ada.</h3>
      </div>
      @endif
      <!-- ==== Sidebar Starts Here ==== -->
      <div class="col-md-4 sidebar"> 
         <!--Sidebar Call to Action
        <div class="sidebar-cta">
          <div> <img src="img/e-book.png" alt="e book"/>
            <h3>Please Download E-book</h3>
            <button class="btn-download">Download</button>
          </div>
        </div>
        <hr>-->

        <!--Sidebar Categories-->
        <h2>Kategori</h2>
        <ul class="sidebar-list">
          @foreach($daftarkategori as $kategori)
            <li><a href="{{ url('pengaduanwarga/'. $kategori->id) }}">{{ $kategori->namakategori }}</a></li>
          @endforeach
        </ul>
        <hr>
    </div>
  </div>
</section>


@stop