@extends('cms.index')
@section('main')
<header class="inner"> 
  <!-- Banner -->
  <div class="header-content">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <h1 id="homeHeading"><a href="index-2.html">Home</a> / Detail Usulan</h1>
        </div>
      </div>
    </div>
  </div>
</header>

<!-- Blog single page -->
<section class="section-bottom-border">
  <div class="container">
    <div class="row">
      <div class="col-md-8 list-container post">
        @include('_partial.flash_message')
        <h3><b>{{ $usulan->judul }}</b></h3> 
        <p align="left"><i class="glyphicon glyphicon-user"></i> <a href="#">{{$usulan->warga->nama}}</a> &nbsp;&nbsp;<i class="glyphicon glyphicon-calendar"></i> {{ $usulan->created_at->format('d-m-Y H:i') }}
        @foreach($fotousulan as $foto)
        <p align='justify'><img width="100%" src="{{ asset('fotoupload/' . $foto->foto) }}"></p>
        @endforeach
        <p align='justify'>{{ $usulan->deskripsi }}</p>
        <div style="width: 100%; height: 400px;">
			  {!! Mapper::render() !!}
		    </div>
        <br>
        <div class="post-social-share"> <span>Bagikan ke</span> 
        <?php 
        $url = "http://sedesa.id/detailusulan/{{ $usulan->id }}";
        ?>
        <a href="https://www.facebook.com/sharer/sharer.php?u={{ urlencode($url) }}" target="_blank"><i class="fa fa-facebook fa-2x" aria-hidden="true"></i></a> 
        <a href="https://twitter.com/intent/tweet?url={{ urlencode($url) }}" target="_blank"><i class="fa fa-twitter fa-2x" aria-hidden="true"></i></a> 
        <a href="https://plus.google.com/share?url={{ urlencode($url) }}" target="_blank"><i class="fa fa-google-plus fa-2x" aria-hidden="true"></i></a> 
        <a href="http://www.linkedin.com/shareArticle?mini=true&url={{ urlencode($url) }}" target="_blank"><i class="fa fa-linkedin fa-2x" aria-hidden="true"></i></a> 
        </div>
        <hr>
        <h3>Daftar Dukungan</h3>
        <ul class="comments-list">
            @foreach($polling as $dukungan)
            <!-- KOMENTAR -->
            <li class="comment"> <a class="pull-left" href="#"> <img class="avatar" src="{{ asset('img/user.png') }}" alt="avatar"> </a>
              <div class="comment-body">
                <div class="comment-heading">
                  <h5 class="user"> <b>{{ $dukungan->nama }}</b> {{ $dukungan->created_at->format('d-m-Y H:i') }} </h5>
                </div>
                @if(count($dukungan->alasan) > 0)
                <p align="justify"><b>Alasan mendukung : </b>{{ $dukungan->alasan }} </p>
                @endif
              </div><br>
            </li>
            @endforeach
        </ul>
      </div>
      <!-- ==== Sidebar Starts Here ==== -->
      <div class="col-md-4 sidebar"> 
      <div class="post-social-share"> <span>Bagikan ke</span> 
        <?php 
        $url = "http://sedesa.id/detailusulan/{{ $usulan->id }}";
        ?>
        <a href="https://www.facebook.com/sharer/sharer.php?u={{ urlencode($url) }}" target="_blank"><i class="fa fa-facebook fa-2x" aria-hidden="true"></i></a> 
        <a href="https://twitter.com/intent/tweet?url={{ urlencode($url) }}" target="_blank"><i class="fa fa-twitter fa-2x" aria-hidden="true"></i></a> 
        <a href="https://plus.google.com/share?url={{ urlencode($url) }}" target="_blank"><i class="fa fa-google-plus fa-2x" aria-hidden="true"></i></a> 
        <a href="http://www.linkedin.com/shareArticle?mini=true&url={{ urlencode($url) }}" target="_blank"><i class="fa fa-linkedin fa-2x" aria-hidden="true"></i></a> 
      </div>
       <!--Sidebar Categories-->
      <h2>Dukungan</h2>
      <p><b>{{ $jumlahpolling }} Orang telah mendukung Usulan ini.</b> Mari kita dukung agar usulan <b>{{ $usulan->warga->nama }}</b> diperhatikan oleh Pemerintah Desa / Pusat.
      </p>

      {!! Form::open(['url' => 'pollingusulan', 'files' => true]) !!}
        {!! Form::hidden('id_usulan', $usulan->id) !!}
        {{-- Nama --}}
          @if($errors->any())
            <div class="form-group {{ $errors->has('nama') ? 'has-error' : 'has-success' }}">
          @else
            <div class="form-group">
          @endif
              {!! Form::label('nama','Nama',['class' => 'control-label']) !!}
              @if(Auth::check())
              {!! Form::text('nama', Auth::user()->name,['class' => 'form-control','readonly']) !!}
              @else
              {!! Form::text('nama', null,['class' => 'form-control']) !!}
              @endif
          @if ($errors->has('nama'))
            <span class="help-block">{{ $errors->first('nama') }}</span>
          @endif
            </div>

        {{-- Email --}}
          @if($errors->any())
            <div class="form-group {{ $errors->has('email') ? 'has-error' : 'has-success' }}">
          @else
            <div class="form-group">
          @endif
              {!! Form::label('email','Email',['class' => 'control-label']) !!}
              @if(Auth::check())
              {!! Form::text('email', Auth::user()->email,['class' => 'form-control','readonly']) !!}
              @else
              {!! Form::text('email', null,['class' => 'form-control']) !!}
              @endif
          @if ($errors->has('email'))
              <span class="help-block">{{ $errors->first('email') }}</span>
          @endif
            </div>
          
        {{-- Alasan --}}
          <div class="form-group">
          {!! Form::label('alasan','Alasan Mendukung',['class' => 'control-label']) !!}
          {!! Form::textarea('alasan', null,['class' => 'form-control']) !!}
          @if ($errors->has('alasan'))
              <span class="help-block">{{ $errors->first('alasan') }}</span>
          @endif
           </div>

        {{-- Submit button --}}
            <div class="">
              {!! Form::submit('Simpan',[ 'buttontype' => 'button','class' => 'btn btn-success' ]) !!}
            </div>
        {!! Form::close() !!}
      <h2>Kategori</h2>
        <ul class="sidebar-list">
          @foreach($daftarkategori as $kategori)
            <li><a href="{{ url('usulan/'. $kategori->id) }}">{{ $kategori->namakategori }}</a></li>
          @endforeach
        </ul>
      </div>
      <!-- ==== Sidebar Ends Here ==== --> 
    </div>
  </div>
</section>
@stop