@extends('cms.index')
@section('main')
<!--Konnect Slider -->
<div class='konnect-carousel carousel-image carousel-image-pagination carousel-image-arrows flexslider'>
  <ul class='slides'>
  @foreach($daftarinformasi as $informasi)
    <!--Slider One-->
    <li class='item'>
      <div class='container'>
        <div class='row pos-rel'>
          <div class='col-sm-12 col-md-6 animate'>
            <h1 class='big fadeInDownBig animated' >{{ $informasi->judul }}</h1>
            <p class='normal fadeInUpBig animated delay-point-five-s' align="left">{{ $informasi->deskripsi }}</p>
            <a class='btn btn-bordered btn-white btn-lg fadeInRightBig animated delay-one-s' href='#'> Selengkapnya </a> </div>
          <div class='col-md-6 animate pos-sta hidden-xs hidden-sm'> <img class="img-responsive img-right fadeInUpBig animated delay-one-point-five-s" alt="sedesa.id" src="cms/img/slider/{{ $informasi->foto }}" /> </div>
        </div>
      </div>
    </li>
  @endforeach  
    <!--Slider Two
    <li class='item'>
      <div class='container'>
        <div class='row pos-rel'>
          <div class='col-md-6 animate pos-sta hidden-xs hidden-sm'> <img class="img-responsive img-left fadeInUpBig animated" alt="Circle" src="cms/img/slider/student-2.png" /> </div>
          <div class='col-sm-12 col-md-6 animate'>
            <h2 class='big fadeInUpBig animated delay-point-five-s'>Based on Bootstrap</h2>
            <p class='normal fadeInDownBig animated delay-one-s'>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris in tincidunt mauris. Etiam arcu enim, laoreet vitae orci vel, rutrum feugiat nibh. Integer feugiat ligula tellus, non pulvinar justo pharetra eu. Nullam vehicula lorem ut diam tincidunt sagittis. Morbi est ligula, posuere in laoreet ac, porta porttitor dui</p>
            <a class='btn btn-bordered btn-white btn-lg fadeInLeftBig animated delay-one-point-five-s' href='#'> Show more </a> </div>
        </div>
      </div>
    </li>-->
    
    <!--Slider Three
    <li class='item'>
      <div class='container'>
        <div class='row pos-rel'>
          <div class='col-sm-12 col-md-6 animate'>
            <h2 class='big fadeInLeftBig animated'>Clean and Flat</h2>
            <p class='normal fadeInRightBig animated delay-point-five-s'>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris in tincidunt mauris. Etiam arcu enim, laoreet vitae orci vel, rutrum feugiat nibh. Integer feugiat ligula tellus, non pulvinar justo pharetra eu. Nullam vehicula lorem ut diam tincidunt sagittis. Morbi est ligula, posuere in laoreet ac, porta porttitor dui</p>
            <a class='btn btn-bordered btn-white btn-lg fadeInUpBig animated delay-one-s' href='#'> Show more </a> </div>
          <div class='col-md-6 animate pos-sta hidden-xs hidden-sm'> <img class="img-responsive img-right fadeInUpBig animated delay-one-point-five-s" alt="Man" src="cms/img/slider/student-3.png" /> </div>
        </div>
      </div>
    </li>-->
  </ul>
</div>
<!--/. Konnect Slider --> 
<!-- EduCourse Stats-->
<section class="light-bg">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h2 class="section-heading">Informasi SeDesa.id</h2>
        <p>Bergabunglah dengan mereka yang sudah berhasil membangun desa nya dengan memanfaatkan fitur di aplikasi kami. Pemerintah pun butuh informasi langsung dari warga desa agar usulan / keluhan bisa didengar. </p>
        <div class="template-space"></div>
      </div>
      <div class="company-stats">
        <div class="col-md-3 col-sm-6">
          <div class="profile-box"> <img src="cms/img/icons/tool.png" alt="icon">
            <h4><span>150+</span> Usulan Diterima</h4>
          </div>
        </div>
        <div class="col-md-3 col-sm-6">
          <div class="profile-box"> <img src="cms/img/icons/expert.png" alt="icon">
            <h4><span>100.000+</span> Warga Desa</h4>
          </div>
        </div>
        <div class="col-md-3 col-sm-6">
          <div class="profile-box"> <img src="cms/img/icons/clients.png" alt="icon">
            <h4><span>500+</span> Desa Tergabung</h4>
          </div>
        </div>
        <div class="col-md-3 col-sm-6">
          <div class="profile-box"> <img src="cms/img/icons/success.png" alt="icon">
            <h4><span>1.000.000+</span> Terima Manfaat</h4>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- Banner Box 
<div class="container banner">
  <div class="row">
    <div class="col-sm-4">
      <div class="banner-bar"> <img src="cms/img/icons/classroom.png" alt="icon">
        <h3><span>Experienced Trainers</span></h3>
        <p>Curabitur ut est a mi fermentum tristique. Aliquam et ante odio. Donec elementum odio eget ex porta, vel laoreet nisl fermentum.</p>
      </div>
    </div>
    <div class="col-sm-4">
      <div class="banner-bar"> <img src="cms/img/icons/certificate.png" alt="icon">
        <h3><span>Certification</span></h3>
        <p>Curabitur ut est a mi fermentum tristique. Aliquam et ante odio. Donec elementum odio eget ex porta, vel laoreet nisl fermentum.</p>
      </div>
    </div>
    <div class="col-sm-4">
      <div class="banner-bar"> <img src="cms/img/icons/job-support.png" alt="icon">
        <h3><span>Job Support</span></h3>
        <p>Curabitur ut est a mi fermentum tristique. Aliquam et ante odio. Donec elementum odio eget ex porta, vel laoreet nisl fermentum.</p>
      </div>
    </div>
  </div>
</div>-->

<!-- Company profile -->
<sectio id="section-login">
  <div class="container">
    <div class="row">
      <div class="col-md-12"> 
        <!--Services Heading--><br>
        <h2 class="section-heading">Tentang Kami</h2>
        <div class="template-space"></div>
      </div>
      <div class="col-md-6">
        <h2 class="para-heading">Apa itu SeDesa.id ?</h2>
        <p>Sebuah gerakan nasional yang mengajak seluruh Warga Desa di seluruh Indonesia untuk berperan aktif membangun desa memanfaatkan kecanggihan teknologi, 
        serta sekaligus berkontribusi dalam pertukaran informasi tentang potensi-potensi di desa. Yang nantinya bisa dipertimbangkan oleh Pemerintah Pusat dalam kajian dana pembangunan di Desa tersebut.</p>
        <p>Sesuai nawacita Presiden Republik Indonesia Bapak Ir. Joko Widodo, membangun Indonesia harus dimulai dari lingkup terkecil yaitu dengan prinsip gotong royong dari Warga untuk Desa, Demi peningkatan 
        kesejahteraan masyarakat dan pembangunan Indonesia yang merata, dari Sabang sampai Merauke dari Miangas sampai Pulau Rote.</p>
        <!--<a class="service-box-button">Daftar Sekarang</a>--> </div>
      <div class="col-md-6"> <img src="cms/img/desa.jpg" class="img-responsive img-hide-sm" alt="Sedesa.id"> </div>
    </div>
  </div><br>
  
  <aside class="theme-bg aside-cta">
    <div class="container text-center">
      <div class="row">
        <div class="col-md-6 col-xs-12 text-white">
          <h3 class="margin-10 text-white">Ingin Desa kamu berkembang ? Daftar sebagai</h3>
        </div>
        <div class="col-sm-2 col-xs-12 text-center text-white purchase-button">
        <a href="{{ url('register') }}" class="template-button"><i class="fa fa-user" aria-hidden="true"></i> Warga </a>
        </div>
        <div class="col-sm-3 col-xs-12 text-center text-white purchase-button">
        <a href="{{ url('daftarperangkat') }}" class="template-button"><i class="fa fa-user-plus" aria-hidden="true"></i> Perangkat Desa </a>
        </div>
      </div>
    </div>
  </aside>
</section>
<!--Call To Action-->

<!--/ Call To Action--> 
<!--Courses-->
<section class="template-news">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h2 class="section-heading text-dark">Informasi Desa</h2>
        <p>Berbagai kategori informasi yang kami sajikan kepada anda,silahkan pilih kategori informasi yang akan anda cari.</p>
        <div class="template-space"></div>
      </div>
    </div>
    <div class="row"> 

    @foreach($daftarkategoripost as $kategori)
      <!--Kategori-->
      <div class="col-sm-4 article-box">
        <article>
          <div class="news-post">
            <div class="img-box"> <!--<span>$150</span>--><a href="{{ url('informasi/'. $kategori->id) }}"><img src="cms/img/news/{{ $kategori->foto }}" alt="it's me Image"></a> </div>
            <div class="post-content-text">
              <h4><span>{{ $kategori->namakategori }}</span></h4>
              <!--<h4><i class="fa fa-calendar-check-o" aria-hidden="true"></i> 3-4 Weeks</h4>-->
              <div class="post-more"><a href="{{ url('informasi/'. $kategori->id) }}">View</a> </div>
            </div>
          </div>
        </article>
      </div>
    @endforeach
    </div>
  </div>
</section>

<!--Testmonials 
<aside class="dark-bg">
  <div class="container">
    <div class="row">
      <div class="col-md-12 text-white">
        <h2 class="section-heading text-white">Student Reviews</h2>
        <div class="template-space"></div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12" data-wow-delay="0.2s">
        <div class="carousel slide" data-ride="carousel" id="quote-carousel"> -->

          <!-- Carousel Slides / Quotes 
          <div class="carousel-inner text-center"> -->
          
            <!--Testmonial One active
            <div class="item active">
              <blockquote>
                <div class="row">
                  <div class="col-md-8 col-md-offset-2 col-xs-12">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. !</p>
                    <small>Someone Client</small> </div>
                </div>
              </blockquote>
            </div>-->
            
            <!--Testmonial Two
            <div class="item">
              <blockquote>
                <div class="row">
                  <div class="col-md-8 col-md-offset-2 col-xs-12">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>
                    <small>Someone Client</small> </div>
                </div>
              </blockquote>
            </div>-->
            
            <!--Testmonial Three
            <div class="item">
              <blockquote>
                <div class="row">
                  <div class="col-md-8 col-md-offset-2 col-xs-12">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. .</p>
                    <small>Someone Client</small> </div>
                </div>
              </blockquote>
            </div>
          </div>-->
          
          <!-- Carousel Buttons Next/Prev  
          <a data-slide="prev" href="#quote-carousel" class="left carousel-control"><i class="fa fa-angle-left"></i></a> <a data-slide="next" href="#quote-carousel" class="right carousel-control"><i class="fa fa-angle-right"></i></a>-->

          <!-- Bottom Carousel Indicators 
          <ol class="carousel-indicators">
            <li data-target="#quote-carousel" data-slide-to="0" class="active"></li>
            <li data-target="#quote-carousel" data-slide-to="1"></li>
            <li data-target="#quote-carousel" data-slide-to="2"></li>
          </ol>
        </div>
      </div>
    </div>
  </div>
</aside>-->

@stop