
<!-- Top Bar  -->
<div class="konnect-info">
  <div class="container-fluid">
    <div class="row"> 
      <!-- Top bar Left -->
      <div class="col-md-6 col-sm-8 hidden-xs">
        <!--<ul>
          <li><i class="fa fa-paper-plane" aria-hidden="true"></i> support@konnectplugins.com </li>
          <li class="li-last"> <i class="fa fa-volume-control-phone" aria-hidden="true"></i> (040) 123-4567</li>
        </ul>-->
      </div>
      <!-- Top bar Right -->
      <div class="col-md-6 col-sm-4">
      @if (Auth::check())
        <ul class="konnect-float-right">
          <li><a href="{{ url('/') }}"><i class="glyphicon glyphicon-user"></i> {{ Auth::user()->name }} </a></li>
          <li><a href="{{ url('logout') }}"><i class="glyphicon glyphicon-off"></i> Logout</a></li>
          <li class="li-last hidden-xs hidden-sm"><a target="_blank" href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a> <a target="_blank" href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a> <a target="_blank" href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a> <a target="_blank" href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a><a href="#"> <i class="fa fa-instagram"></i> </a></li>
        </ul>
      @else
        <ul class="konnect-float-right">
          <li><a href="{{ url('login') }}"><i class="fa fa-user-o" aria-hidden="true"></i> Masuk </a></li>
          <li><a href="#section-login"><i class="fa fa-file-text-o" aria-hidden="true"></i> Daftar </a></li>
          <li class="li-last hidden-xs hidden-sm"><a target="_blank" href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a> <a target="_blank" href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a> <a target="_blank" href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a> <a target="_blank" href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a><a href="#"> <i class="fa fa-instagram"></i> </a></li>
        </ul>
      @endif
      </div>
    </div>
  </div>
</div>
<!-- Main Navigation + LOGO Area -->
<nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
  <div class="container-fluid">
    <div class="navbar-header"> 
      <!-- Responsive Menu -->
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> <span class="sr-only">Toggle navigation</span> <img src="cms/img/icons/menu.png" alt="menu" width="40"> </button>
      <!-- Logo --> 
      <a class="navbar-brand" href="{{ url('home') }}"><img class="logo-change" src="{{ asset ('cms/img/logo-green2.png')}}" alt="logo"></a> </div>
    
    <!-- Menu Items -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
        <li class="active"><a href="{{ url('home') }}">Home</a></li>
        <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Informasi <i class="fa fa-angle-down" aria-hidden="true"></i></a>
          <ul class="dropdown-menu">
          @foreach($daftarkategoripost as $kategoripost)
            <li><a href="{{ url('informasi/'. $kategoripost->id) }}">{{ $kategoripost->namakategori }}</a></li>
          @endforeach
          </ul>
        </li>
        <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Peran Warga <i class="fa fa-angle-down" aria-hidden="true"></i></a>
          <ul class="dropdown-menu">
            <li><a href="{{ url('usulanwarga') }}">Usulan</a></li>
            <li><a href="{{ url('pengaduanwarga') }}">Pengaduan</a></li>
          </ul>
        </li>
        <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Kependudukan <i class="fa fa-angle-down" aria-hidden="true"></i></a>
          <ul class="dropdown-menu">
            <li><a href="#">Pelayanan</a></li>
            <li><a href="#">Pemetaan Penduduk</a></li>
          </ul>
        </li>
        <li><a href="#">Toko Desa</a></li>
        <li class="search-icon"><a href="javascript:void(0)"><i class="fa fa-search" aria-hidden="true"></i></a>
          <div class="search-form">
            <form class="navbar-form" role="search">
              <div class="input-group add-on">
                <input class="form-control" placeholder="Pencarian.." name="srch-term" id="srch-term" type="text">
                <div class="input-group-btn">
                  <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                </div>
              </div>
            </form>
          </div>
        </li>
      </ul>
    </div>
    <!-- /.navbar-collapse --> 
  </div>
  <!-- /.container-fluid --> 
</nav>