@extends('cms.index')
@section('main')
<header class="inner"> 
  <!-- Banner -->
  <div class="header-content">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <h1 id="homeHeading"><a href="index-2.html">Home</a> / Detail Informasi</h1>
        </div>
      </div>
    </div>
  </div>
</header>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-dialog">
	<!-- Modal Produk-->
	<div class="modal-content">
		<div class="modal-header">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
      <div><b>Balas Komentar</b></div>
    </div>
    <div class="modal-body">
			{!! Form::open(['url' => 'balasan', 'files' => true, 'class' => 'form-horizontal']) !!}
			{!! Form::hidden('id_posting', $posting->id) !!}
      {!! Form::hidden('id_komentarpost', null,['class' => 'form-control','id'=>'id_komentarpost']) !!}
      &nbsp;
      <div class="form-group">
        {!! Form::label('nama','Nama',['class' => 'col-sm-2 control-label']) !!}
        <div class="col-sm-9">
        @if(Auth::check())
        {!! Form::text('nama', Auth::user()->name,['class' => 'form-control','readonly']) !!}
        @else
        {!! Form::text('nama', null,['class' => 'form-control']) !!}
        @endif
        </div>
			</div>
      <div class="form-group">
        {!! Form::label('email','Email',['class' => 'col-sm-2 control-label']) !!}
        <div class="col-sm-9">
        @if(Auth::check())
        {!! Form::text('email', Auth::user()->email,['class' => 'form-control','readonly']) !!}
        @else
        {!! Form::text('email', null,['class' => 'form-control']) !!}
        @endif
        </div>
			</div>
			<div class="form-group">
        {!! Form::label('komentar','Komentar',['class' => 'col-sm-2 control-label']) !!}
        <div class="col-sm-9">
        {!! Form::textarea('komentar', null,['class' => 'form-control']) !!}
        </div>
			</div>
			{{-- Submit button --}}
			<div class="form-group">
        <div class="col-sm-2">
        </div>
        <div class="col-sm-3">
				{!! Form::submit('Simpan',['class' => 'btn btn-primary form-control']) !!}
        </div>
			</div>	
			{!! Form::close() !!}	
		</div>
	</div>
</div>
</div>
<!-- End Modal -->

<!-- Blog single page -->
<section class="section-bottom-border">
  <div class="container">
    <div class="row">
      <div class="col-md-8 list-container post">
        @include('_partial.flash_message')
        <h3><b>{{ $posting->judul }}</b></h3>
        <p align='left'><i class="glyphicon glyphicon-user"></i> <a href="#">{{ $posting->user->name }}</a> &nbsp;&nbsp;<i class="glyphicon glyphicon-calendar"></i> {{ $posting->created_at->format('d-m-Y H:i') }} &nbsp;&nbsp;<i class="glyphicon glyphicon-star"></i> Kategori : <b>{{ $posting->kategoripost->namakategori  }} </b></p>
        <p align='justify'><img width="100%" src="{{ asset('fotoupload/' . $posting->foto) }}"></p> 
        {!! $posting->isipost !!}
        <!-- post tags -->
        <h3>Tags</h3>
        <div class="sidebar-tags"> 
        <?php 
        $tags = explode(',', $posting->tag);
        foreach($tags as $tag){
        echo "<a href='../tags/". $tag ."'>". $tag ."</a>";
        }
        ?>
        </div>
        <br>
        <!-- post social sharing icons -->
        <div class="post-social-share"> <span>Bagikan ke</span> 
        <?php 
        $url = "http://sedesa.id/detailinformasi/{{ $posting->judulseo }}";
        ?>
        <a href="https://www.facebook.com/sharer/sharer.php?u={{ urlencode($url) }}" target="_blank"><i class="fa fa-facebook fa-2x" aria-hidden="true"></i></a> 
        <a href="https://twitter.com/intent/tweet?url={{ urlencode($url) }}" target="_blank"><i class="fa fa-twitter fa-2x" aria-hidden="true"></i></a> 
        <a href="https://plus.google.com/share?url={{ urlencode($url) }}" target="_blank"><i class="fa fa-google-plus fa-2x" aria-hidden="true"></i></a> 
        <!--<a href=”whatsapp://send” data-text="{{ $posting->judul }}" data-href="{{ urlencode($url) }}"><i class="fa fa-whatsapp fa-2x" aria-hidden="true"></i></a>--> 
        <a href="http://www.linkedin.com/shareArticle?mini=true&url={{ urlencode($url) }}" target="_blank"><i class="fa fa-linkedin fa-2x" aria-hidden="true"></i></a> 
        </div>
        <hr>
        <!-- Pager 
        <ul class="pager">
          <li class="prev"> <a href="javascript:void(0)">&larr; Prev</a> </li>
          <li class="next"> <a href="javascript:void(0)">Next &rarr;</a> </li>
        </ul>-->
        <!-- comments -->
        <h3>Komentar</h3>
        <div class="post-footer">
        {!! Form::open(['url' => 'komentar', 'files' => true]) !!}
        {!! Form::hidden('id_posting', $posting->id) !!}
        {{-- Nama --}}
          @if($errors->any())
            <div class="form-group {{ $errors->has('nama') ? 'has-error' : 'has-success' }}">
          @else
            <div class="form-group">
          @endif
              {!! Form::label('nama','Nama',['class' => 'control-label']) !!}
              @if(Auth::check())
              {!! Form::text('nama', Auth::user()->name,['class' => 'form-control','readonly']) !!}
              @else
              {!! Form::text('nama', null,['class' => 'form-control']) !!}
              @endif
          @if ($errors->has('nama'))
            <span class="help-block">{{ $errors->first('nama') }}</span>
          @endif
            </div>

        {{-- Email --}}
          @if($errors->any())
            <div class="form-group {{ $errors->has('email') ? 'has-error' : 'has-success' }}">
          @else
            <div class="form-group">
          @endif
              {!! Form::label('email','Email',['class' => 'control-label']) !!}
              @if(Auth::check())
              {!! Form::text('email', Auth::user()->email,['class' => 'form-control','readonly']) !!}
              @else
              {!! Form::text('email', null,['class' => 'form-control']) !!}
              @endif
          @if ($errors->has('email'))
              <span class="help-block">{{ $errors->first('email') }}</span>
          @endif
            </div>
          
        {{-- Komentar --}}
          @if($errors->any())
            <div class="form-group {{ $errors->has('komentar') ? 'has-error' : 'has-success' }}">
          @else
            <div class="form-group">
          @endif
              {!! Form::label('komentar','Komentar',['class' => 'control-label']) !!}
              {!! Form::textarea('komentar', null,['class' => 'form-control']) !!}
          @if ($errors->has('komentar'))
              <span class="help-block">{{ $errors->first('komentar') }}</span>
          @endif
            </div>

        {{-- Submit button --}}
            <div class="">
              {!! Form::submit('Simpan',[ 'buttontype' => 'button','class' => 'btn btn-success' ]) !!}
            </div>
        {!! Form::close() !!} <br>

          <ul class="comments-list">
            @foreach($daftarkomentar as $komentar)
            <!-- KOMENTAR -->
            <li class="comment"> <a class="pull-left" href="#"> <img class="avatar" src="{{ asset('img/user.png') }}" alt="avatar"> </a>
              <div class="comment-body">
                <div class="comment-heading">
                  <h4 class="user"> {{ $komentar->nama }} </h4>
                  <h5 class="time"> {{ $komentar->created_at->format('d-m-Y H:i') }} </h5>
                </div>
                <p align="justify"> {{ $komentar->komentar }} </p>
                <div class="box-button"> 
					      {{ link_to('#','Balas',['class' => 'btn btn-primary btn-sm','id'=>'tbbalas'.$komentar->id]) }}</div>
              </div>
               @foreach($komentar->balaskomentar as $balasan)
              <!-- BALASAN KOMENTAR -->
              <ul class="comments-list">
                <li class="comment"> <a class="pull-left" href="#"> <img class="avatar" src="{{ asset('img/user.png') }}" alt="avatar"> </a>
                  <div class="comment-body">
                    <div class="comment-heading">
                      <h4 class="user">{{ $balasan->nama }}</h4>
                      <h5 class="time">{{  $balasan->created_at->format('d-m-Y H:i') }}</h5>
                    </div>
                    <p align="justify"> {{  $balasan->komentar }} </p>
                  </div>
                </li>
              </ul>
              <!-- AKHIR BALASAN KOMENTAR -->
              @endforeach
            </li>
            <!-- AKHIR KOMENTAR-->
            @endforeach
          </ul>

        </div>
        <!-- comments end --> 
      </div>
      <!-- ==== Sidebar Starts Here ==== -->
      <div class="col-md-4 sidebar"> 
      <!-- post social sharing icons -->
        <div class="post-social-share"> <span>Bagikan ke</span> 
        <?php 
        $url = "http://sedesa.id/detailinformasi/{{ $posting->judulseo }}";
        ?>
        <a href="https://www.facebook.com/sharer/sharer.php?u={{ urlencode($url) }}" target="_blank"><i class="fa fa-facebook fa-2x" aria-hidden="true"></i></a> 
        <a href="https://twitter.com/intent/tweet?url={{ urlencode($url) }}" target="_blank"><i class="fa fa-twitter fa-2x" aria-hidden="true"></i></a> 
        <a href="https://plus.google.com/share?url={{ urlencode($url) }}" target="_blank"><i class="fa fa-google-plus fa-2x" aria-hidden="true"></i></a> 
        <!--<a href=”whatsapp://send” data-text="{{ $posting->judul }}" data-href="{{ urlencode($url) }}"><i class="fa fa-whatsapp fa-2x" aria-hidden="true"></i></a>--> 
        <a href="http://www.linkedin.com/shareArticle?mini=true&url={{ urlencode($url) }}" target="_blank"><i class="fa fa-linkedin fa-2x" aria-hidden="true"></i></a> 
        </div>
        <!--Sidebar Popular Posts-->
        <h2>Informasi Populer</h2>
        @foreach($daftarpopuler as $populer)
        <div class="sidebar-post"> <a href="{{ url('detailinformasi/'. $populer->judulseo) }}">{{ $populer->judul }}</a>
          <p class="post-meta">&nbsp;&nbsp;&nbsp;&nbsp;Diposting Oleh <a href="#">{{$populer->user->name}}</a> {{$populer->created_at->format('d-m-Y H:i')}}</p>
        </div>
        @endforeach
        <!--Sidebar Call to Action
        <div class="sidebar-cta">
          <div> <img src="img/e-book.png" alt="e book"/>
            <h3>Please Download E-book</h3>
            <button class="btn-download">Download</button>
          </div>
        </div>
        <hr>-->
        <!--Sidebar Categories-->
        <h2>Kategori</h2>
        <ul class="sidebar-list">
          @foreach($daftarkategoripost as $kategoripost)
            <li><a href="{{ url('informasi/'. $kategoripost->id) }}">{{ $kategoripost->namakategori }}</a></li>
          @endforeach
        </ul>
        <hr>
        <!--Sidebar Popular Tags-->
        <h2>Tags Populer</h2>
        <div class="sidebar-tags"> 
        <a href="{{ url('tags/desa') }}">Desa</a> <a href="{{ url('tags/bumdes') }}">Bumdes</a><a href="{{ url('tags/bali') }}">Bali</a>
        <a href="{{ url('tags/pembangunan') }}">Pembagunan</a><a href="{{ url('tags/danadesa') }}">Dana Desa</a><a href="{{ url('tags/jokowi') }}">Jokowi</a>
        <a href="{{ url('tags/dusun') }}">Dusun</a><a href="{{ url('tags/banjar') }}">Banjar</a>
        </div>
        </div>
      <!-- ==== Sidebar Ends Here ==== --> 
    </div>
  </div>
</section>
<!-- jQuery --> 
<script src="{{ asset ('cms/assets/jquery/jquery.min.js')}}"></script> 
<script>
$(document).ready(function(){
	<?php
	echo "var daftarkomentar ={$daftarkomentar}; ";
	?>
	var i = 0;
    for (i = 0; i < daftarkomentar.length; i++) {
    (function(i) {
        $("#tbbalas" + daftarkomentar[i].id).click(function() {
			document.getElementById("id_komentarpost").value =  daftarkomentar[i].id;
			$("#myModal").modal({backdrop: "static"});
        });
      })(i);
    }
});
</script>
@stop