<div id="footer">
<!--Main footer-->
<section class="main-footer">
  <div class="container">
    <div class="row"> 
      <!--footer widget one-->
      <div class="col-md-4 col-sm-6">
        <div class="footer-widget"> <img src="{{ asset ('cms/img/logo-green2.png')}}" alt="" class="img-responsive logo-change">
          <p>Portal Informasi warga Desa seluruh Indonesia, dari Warga untuk Desa demi pembangunan Indonesia yang merata.</p>
          <span><a href="#" class="read-more">Selengkapnya</a></span> </div>
      </div>
      <!--/ footer widget one--> 
      
      <!--footer widget Two-->
      <div class="col-md-4 col-sm-6">
        <div class="footer-widget address">
          <h3>Kontak Kami</h3>
          <p><i class="fa fa-address-card-o" aria-hidden="true"></i> <span>Jl.Gringsing 2 No.24 <br>
            Tlogosari, Semarang - Indonesia</span></p>
          <p><i class="fa fa-envelope-o" aria-hidden="true"></i> <span>info@sedesa.id</span></p>
          <p><i class="fa fa-volume-control-phone" aria-hidden="true"></i> <span>+62 101 0000 000 <br>
            +62 202 0000 001</span></p>
        </div>
      </div>
      <!--/ footer widget Two--> 
      
      <!--footer widget Three-->
      <div class="col-md-4 col-sm-6">
        <div class="footer-widget quicl-links">
          <h3>Menu</h3>
          <ul>
            <li><i class="fa  fa-angle-right"></i> <a href="#">Usulan</a></li>
            <li><i class="fa  fa-angle-right"></i> <a href="#">Pengaduan</a></li>
            <li><i class="fa  fa-angle-right"></i> <a href="#">Berita</a></li>
            <li><i class="fa  fa-angle-right"></i> <a href="#">Event</a></li>
            <li><i class="fa  fa-angle-right"></i> <a href="{{ url('login') }}">Masuk</a></li>
            <li><i class="fa  fa-angle-right"></i> <a href="{{ url('register') }}">Daftar</a></li>
            <li><i class="fa  fa-angle-right"></i> <a href="#">Syarat dan Ketentuan</a></li>
            <li><i class="fa  fa-angle-right"></i> <a href="#">Kebijakan Privasi</a></li>
          </ul>
        </div>
      </div>
      <!--/ footer widget thre--> 
    </div>
  </div>
</section>
<!--/Main Footer--> 

<!--copyright Footer-->
<footer>
  <div class="container">
    <div class="row">
      <div class="col-md-6 col-sm-6 text-left"> 
        
        <!--Footer Social Icons-->
        <div class="contact-social">
          <p><a href="#"> <i class="fa fa-twitter"></i> </a> <a href="#"> <i class="fa fa-facebook"></i> </a> <a href="#"> <i class="fa fa-google-plus"></i> </a> <a href="#"> <i class="fa fa-rss"></i> </a> <a href="#"> <i class="fa fa-instagram"></i> </a></p>
        </div>
      </div>
      
      <!-- Footer Copy rights-->
      <div class="col-md-6 col-sm-6 text-right">
        <p> &copy; Copyright <?php echo date("Y"); ?>  Sedesa.id </p>
      </div>
    </div>
  </div>
</footer>
</div>