@extends('template')
@section('main')
<div id="usulan" class="panel panel-default">
<b><h4>&nbsp;&nbsp;Usulan Warga</h4></b><hr>
@if (isset($usulan))
	{!! Form::model($usulan, ['method' => 'PATCH', 'action' => ['UsulanwebController@verifikasi', $usulan->id],'files'=>true]) !!}
	{!! Form::hidden('id', $usulan->id) !!}
	{!! Form::hidden('id_warga', $usulan->id_warga) !!}
		<div class="panel-body">
	{{-- Verifikasi --}}
	<div class="form-group">
	{!! Form::label('status','Status Verifikasi',['class' => 'control-label']) !!}
		<div class="radio">
		<label>
		{!! Form::radio('status','1') !!} Direview
		</label>
		</div>
		<div class="radio">
		<label>
		{!! Form::radio('status','2') !!} Diterima
		</label>
		</div>
		<div class="radio">
		<label>{!! Form::radio('status','3') !!} Ditolak
		</label>
		</div>
	</div>
	{{-- Status --}}
	<div class="form-group">
	{!! Form::label('prioritas','Prioritaskan',['class' => 'control-label']) !!}
		<div class="radio">
		<label>
		{!! Form::radio('prioritas','2') !!} Ya
		</label>
		</div>
		<div class="radio">
		<label>{!! Form::radio('prioritas','1') !!} Tidak
		</label>
		</div>
	</div>
	{{-- Tombol --}}
	<div class="row">
		<div class="col-md-3">
		<div class="form-group">
		{!! Form::submit('Konfirm Verifikasi',['class' => 'btn btn-success form-control']) !!}
		</div>
		</div>
	</div>
	{!! Form::close() !!}
@endif

		<table class="table table-striped">
		<tr><th>Tanggal Usulan</th><td>{{ $usulan->tanggal }}
		</td></tr>
		<tr><th>Nama Warga</th><td>{{ $usulan->warga->nama }}
		</td></tr>
		<tr><th>Judul Usulan</th><td>{{ $usulan->judul }}</td></tr>
		<tr><th>Kategori</th><td>{{ $usulan->kategori->namakategori }}
		</td></tr>
		<tr><th>Lokasi</th>
		<td>
		<div style="width: 400px; height: 400px;">
			{!! Mapper::render() !!}
		</div>
		</td></tr>
		<tr><th>Foto</th><td>
		@foreach($fotousulan as $foto)
		<img width="300" height="300" src="{{ asset('fotoupload/' . $foto->foto) }}">
		@endforeach
		</td></tr>
		<tr><th>Volume</th><td>{{ $usulan->volume }}</td></tr>
		<tr><th>Satuan</th><td>{{ $usulan->satuan }}</td></tr>
		<tr><th>Penerima Manfaat</th><td></td></tr>
		<tr><th>- Laki-Laki</th><td>{{ $usulan->pria }}</td></tr>
		<tr><th>- Perempuan</th><td>{{ $usulan->wanita }}</td></tr>
		<tr><th>- Rumah Tangga Miskin</th><td>{{ $usulan->rtm }}</td></tr>
		<tr><th>Deskripsi</th><td>{{ $usulan->deskripsi }}
		</td></tr>
		<tr><th>Status</th><td>
			@if( $usulan->status == 1 )
			Direview
			@elseif( $usulan->status == 2 )
			Disetujui
			@elseif( $usulan->status == 3 )
			Ditolak
			@endif
		</td></tr>
		</table>
	</div>
	</div>
</div>
@stop

@section('footer')
@include('footer')
@stop