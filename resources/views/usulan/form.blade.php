@if (isset($usulan))
{!! Form::hidden('id', $usulan->id) !!}
@endif

{{-- Tanggal --}}
@if($errors->any())
<div class="form-group {{ $errors->has('tanggal') ? 'has-error' : 'has-success' }}"></div>
@else
<div class="form-group">
@endif
	{!! Form::label('tanggal','Tanggal',['class' => 'control-label']) !!}
	{!! Form::date('tanggal', null,['class' => 'form-control']) !!}
	@if ($errors->has('tanggal'))
	<span class="help-block">{{ $errors->first('tanggal') }}</span>
	@endif
</div>

{{-- Judul --}}
@if($errors->any())
<div class="form-group {{ $errors->has('judul') ? 'has-error' : 'has-success' }}"></div>
@else
<div class="form-group">
@endif
	{!! Form::label('judul','Judul',['class' => 'control-label']) !!}
	{!! Form::text('judul', null,['class' => 'form-control']) !!}
	@if ($errors->has('judul'))
	<span class="help-block">{{ $errors->first('judul') }}</span>
	@endif
</div>

{{--  Kategori --}}
<div class="form-group">
	{!! Form::label('id_kategori','Kategori',['class' => 'control-label']) !!}
	@if(count($daftarkategori) > 0)
	{!! Form::select('id_kategori', $daftarkategori, null,['class' => 'form-control js-example-basic-single', 'id'=>'id_kategori','placeholder'=>'Pilih Kategori']) !!}
	@else
	<p>Tidak ada pilihan kategori,silahkan buat dulu.</p>
	@endif
	@if ($errors->has('id_kategori'))
	<span class="help-block">{{ $errors->first('id_kategori') }}</span>
	@endif
</div>

{{--  Warga --}}
<div class="form-group">
	{!! Form::label('id_warga','Warga',['class' => 'control-label']) !!}
	@if(count($daftarwarga) > 0)
	{!! Form::select('id_warga', $daftarwarga, null,['class' => 'form-control js-example-basic-single', 'id'=>'id_warga','placeholder'=>'Pilih Warga']) !!}
	@else
	<p>Tidak ada pilihan Warga,silahkan buat dulu.</p>
	@endif
	@if ($errors->has('id_warga'))
	<span class="help-block">{{ $errors->first('id_warga') }}</span>
	@endif
</div>


{{-- Volume --}}
@if($errors->any())
<div class="form-group {{ $errors->has('volume') ? 'has-error' : 'has-success' }}"></div>
@else
<div class="form-group">
@endif
	{!! Form::label('volume','Volume',['class' => 'control-label']) !!}
	{!! Form::text('volume', null,['class' => 'form-control']) !!}
	@if ($errors->has('volume'))
	<span class="help-block">{{ $errors->first('volume') }}</span>
	@endif
</div>

{{-- Satuan --}}
@if($errors->any())
<div class="form-group {{ $errors->has('satuan') ? 'has-error' : 'has-success' }}"></div>
@else
<div class="form-group">
@endif
	{!! Form::label('satuan','Satuan',['class' => 'control-label']) !!}
	{!! Form::text('satuan', null,['class' => 'form-control']) !!}
	@if ($errors->has('satuan'))
	<span class="help-block">{{ $errors->first('satuan') }}</span>
	@endif
</div>

{{-- Deskripsi --}}
@if($errors->any())
<div class="form-group {{ $errors->has('deskrpsi') ? 'has-error' : 'has-success' }}"></div>
@else
<div class="form-group">
@endif
	{!! Form::label('deskripsi','Detail Usulan',['class' => 'control-label']) !!}
	{!! Form::textarea('deskripsi', null,['class' => 'form-control']) !!}
	@if ($errors->has('deskripsi'))
	<span class="help-block">{{ $errors->first('deskripsi') }}</span>
	@endif
</div>

{{-- Status --}}
@if($errors->any())
<div class="form-group {{ $errors->has('status') ? 'has-error' : 'has-success' }}"></div>
@else
<div class="form-group">
@endif
{!! Form::label('status','Status',['class' => 'control-label']) !!}
	<div class="radio">
	<label>
	{!! Form::radio('status','1') !!} Pending
	</label>
	</div>
	<div class="radio">
	<label>{!! Form::radio('status','2') !!} Disetujui
	</label>
	</div>
	<div class="radio">
	<label>{!! Form::radio('status','3') !!} Ditolak
	</label>
	</div>
	@if ($errors->has('status'))
	<span class="help-block">{{ $errors->first('status') }}</span>
	@endif
</div>

{{-- Submit button --}}
<div class="form-group">
	{!! Form::submit($submitButtonText,['class' => 'btn btn-primary form-control']) !!}
</div>

<script>
$(document).ready(function(){
	$('.js-example-basic-single').select2();
});
</script>