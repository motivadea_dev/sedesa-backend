@extends('template')

@section('main')
	<div id="keluarga" class="panel panel-default">
		<div class="panel-heading"><b><h4>Tambah Keluarga</h4></b></div>
		<div class="panel-body">
		{!! Form::open(['url' => 'keluarga', 'files' => true]) !!}

		@include('keluarga.form', ['submitButtonText' => 'Tambah Keluarga'])

		{!! Form::close() !!}
		</div>
	</div>
@stop

@section('footer')
	@include('footer')
@stop