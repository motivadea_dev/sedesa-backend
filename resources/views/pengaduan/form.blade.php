@if (isset($pengaduan))
{!! Form::hidden('id', $pengaduan->id) !!}
@endif

{{-- Tanggal --}}
@if($errors->any())
<div class="form-group {{ $errors->has('tanggal') ? 'has-error' : 'has-success' }}"></div>
@else
<div class="form-group">
@endif
	{!! Form::label('tanggal','Tanggal',['class' => 'control-label']) !!}
	{!! Form::date('tanggal', null,['class' => 'form-control']) !!}
	@if ($errors->has('tanggal'))
	<span class="help-block">{{ $errors->first('tanggal') }}</span>
	@endif
</div>

{{-- Judul --}}
@if($errors->any())
<div class="form-group {{ $errors->has('judul') ? 'has-error' : 'has-success' }}"></div>
@else
<div class="form-group">
@endif
	{!! Form::label('judul','Judul',['class' => 'control-label']) !!}
	{!! Form::text('judul', null,['class' => 'form-control']) !!}
	@if ($errors->has('judul'))
	<span class="help-block">{{ $errors->first('judul') }}</span>
	@endif
</div>

{{--  Kategori Pengaduan --}}
<div class="form-group">
	{!! Form::label('id_kategoripengaduan','Kategori Pengaduan',['class' => 'control-label']) !!}
	@if(count($daftarkategoripengaduan) > 0)
	{!! Form::select('id_kategoripengaduan', $daftarkategoripengaduan, null,['class' => 'form-control js-example-basic-single', 'id'=>'id_kategoripengaduan','placeholder'=>'Pilih Kategori']) !!}
	@else
	<p>Tidak ada pilihan kategori,silahkan buat dulu.</p>
	@endif
	@if ($errors->has('id_kategoripengaduan'))
	<span class="help-block">{{ $errors->first('id_kategoripengaduan') }}</span>
	@endif
</div>

{{--  Dusun --}}
<div class="form-group">
	{!! Form::label('id_dusun','Dusun',['class' => 'control-label']) !!}
	@if(count($daftardusun) > 0)
	{!! Form::select('id_dusun', $daftardusun, null,['class' => 'form-control js-example-basic-single', 'id'=>'id_dusun','placeholder'=>'Pilih Dusun']) !!}
	@else
	<p>Tidak ada pilihan dusun,silahkan buat dulu.</p>
	@endif
	@if ($errors->has('id_dusun'))
	<span class="help-block">{{ $errors->first('id_dusun') }}</span>
	@endif
</div>

{{--  Warga --}}
<div class="form-group">
	{!! Form::label('id_warga','Warga',['class' => 'control-label']) !!}
	@if(count($daftarwarga) > 0)
	{!! Form::select('id_warga', $daftarwarga, null,['class' => 'form-control js-example-basic-single', 'id'=>'id_warga','placeholder'=>'Pilih Warga']) !!}
	@else
	<p>Tidak ada pilihan Warga,silahkan buat dulu.</p>
	@endif
	@if ($errors->has('id_warga'))
	<span class="help-block">{{ $errors->first('id_warga') }}</span>
	@endif
</div>

{{-- Deskripsi --}}
@if($errors->any())
<div class="form-group {{ $errors->has('deskrpsi') ? 'has-error' : 'has-success' }}"></div>
@else
<div class="form-group">
@endif
	{!! Form::label('deskripsi','Detail Pengaduan',['class' => 'control-label']) !!}
	{!! Form::textarea('deskripsi', null,['class' => 'form-control']) !!}
	@if ($errors->has('deskripsi'))
	<span class="help-block">{{ $errors->first('deskripsi') }}</span>
	@endif
</div>

{{-- Privasi Pengaduan --}}
@if($errors->any())
<div class="form-group {{ $errors->has('privasipengaduan') ? 'has-error' : 'has-success' }}"></div>
@else
<div class="form-group">
@endif
{!! Form::label('privasipengaduan','Privasi Pengaduan',['class' => 'control-label']) !!}
	<div class="radio">
	<label>
	{!! Form::radio('privasipengaduan','1') !!} Public
	</label>
	</div>
	<div class="radio">
	<label>{!! Form::radio('privasipengaduan','2') !!} Private
	</label>
	</div>
	@if ($errors->has('privasipengaduan'))
	<span class="help-block">{{ $errors->first('privasipengaduan') }}</span>
	@endif
</div>

{{-- Privasi Identitas --}}
@if($errors->any())
<div class="form-group {{ $errors->has('privasiidentitas') ? 'has-error' : 'has-success' }}"></div>
@else
<div class="form-group">
@endif
{!! Form::label('privasiidentitas','Privasi Identitas',['class' => 'control-label']) !!}
	<div class="radio">
	<label>
	{!! Form::radio('privasiidentitas','1') !!} Public
	</label>
	</div>
	<div class="radio">
	<label>{!! Form::radio('privasiidentitas','2') !!} Private
	</label>
	</div>
	@if ($errors->has('privasiidentitas'))
	<span class="help-block">{{ $errors->first('privasiidentitas') }}</span>
	@endif
</div>

{{-- Status --}}
@if($errors->any())
<div class="form-group {{ $errors->has('status') ? 'has-error' : 'has-success' }}"></div>
@else
<div class="form-group">
@endif
{!! Form::label('status','Status',['class' => 'control-label']) !!}
	<div class="radio">
	<label>
	{!! Form::radio('status','1') !!} Awal
	</label>
	</div>
	<div class="radio">
	<label>{!! Form::radio('status','2') !!} Progres
	</label>
	</div>
	<div class="radio">
	<label>{!! Form::radio('status','3') !!} Selesai
	</label>
	</div>
	@if ($errors->has('status'))
	<span class="help-block">{{ $errors->first('status') }}</span>
	@endif
</div>

{{-- Submit button --}}
<div class="form-group">
	{!! Form::submit($submitButtonText,['class' => 'btn btn-primary form-control']) !!}
</div>

<script>
$(document).ready(function(){
	$('.js-example-basic-single').select2();
});
</script>