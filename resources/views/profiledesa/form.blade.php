@if (isset($profiledesa))
{!! Form::hidden('id', $profiledesa->id) !!}
@endif


{{-- Nama --}}
@if($errors->any())
<div class="form-group {{ $errors->has('nama') ? 'has-error' : 'has-success' }}"></div>
@else
<div class="form-group">
@endif
	{!! Form::label('nama','Nama Desa',['class' => 'control-label']) !!}
	{!! Form::text('nama', null,['class' => 'form-control']) !!}
	@if ($errors->has('nama'))
	<span class="help-block">{{ $errors->first('nama') }}</span>
	@endif
</div>

{{-- Kecamatan --}}
@if($errors->any())
<div class="form-group {{ $errors->has('kecamatan') ? 'has-error' : 'has-success' }}"></div>
@else
<div class="form-group">
@endif
	{!! Form::label('kecamatan','Nama Kecamatan',['class' => 'control-label']) !!}
	{!! Form::text('kecamatan', null,['class' => 'form-control']) !!}
	@if ($errors->has('kecamatan'))
	<span class="help-block">{{ $errors->first('kecamatan') }}</span>
	@endif
</div>

{{-- Kabupaten --}}
@if($errors->any())
<div class="form-group {{ $errors->has('kabupaten') ? 'has-error' : 'has-success' }}"></div>
@else
<div class="form-group">
@endif
	{!! Form::label('kabupaten','Nama Kabupaten',['class' => 'control-label']) !!}
	{!! Form::text('kabupaten', null,['class' => 'form-control']) !!}
	@if ($errors->has('kabupaten'))
	<span class="help-block">{{ $errors->first('kabupaten') }}</span>
	@endif
</div>

{{-- Provinsi --}}
@if($errors->any())
<div class="form-group {{ $errors->has('provinsi') ? 'has-error' : 'has-success' }}"></div>
@else
<div class="form-group">
@endif
	{!! Form::label('provinsi','Nama Provinsi',['class' => 'control-label']) !!}
	{!! Form::text('provinsi', null,['class' => 'form-control']) !!}
	@if ($errors->has('provinsi'))
	<span class="help-block">{{ $errors->first('provinsi') }}</span>
	@endif
</div>

{{-- No Induk Penduduk --}}
@if($errors->any())
<div class="form-group {{ $errors->has('nip') ? 'has-error' : 'has-success' }}"></div>
@else
<div class="form-group">
@endif
	{!! Form::label('nip','NIP',['class' => 'control-label']) !!}
	{!! Form::text('nip', null,['class' => 'form-control']) !!}
	@if ($errors->has('nip'))
	<span class="help-block">{{ $errors->first('nip') }}</span>
	@endif
</div>

{{-- Nama Kepala Desa --}}
@if($errors->any())
<div class="form-group {{ $errors->has('nama_kades') ? 'has-error' : 'has-success' }}"></div>
@else
<div class="form-group">
@endif
	{!! Form::label('nama_kades','Nama Kepalan Desa',['class' => 'control-label']) !!}
	{!! Form::text('nama_kades', null,['class' => 'form-control']) !!}
	@if ($errors->has('nama_kades'))
	<span class="help-block">{{ $errors->first('nama_kades') }}</span>
	@endif
</div>

{{-- Submit button --}}
<div class="form-group">
	{!! Form::submit($submitButtonText,['class' => 'btn btn-primary form-control']) !!}
</div>