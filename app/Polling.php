<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Polling extends Model
{
    protected $table = 'polling';

    protected $fillable = [
        'id_usulan',
        'tanggal',
        'nama',
        'email',  
        'alasan',	        
    	'created_at',
        'updated_at'
    ];

 	public function usulan(){
        return $this->belongsTo('App\Usulan', 'id_usulan');
    }
}
