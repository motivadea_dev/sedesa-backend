<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kategori extends Model
{
    protected $table = 'kategori';

    protected $fillable = [
        'id_profiledesa',
    	'namakategori',        
    	'created_at',
        'updated_at'
    ];

    public function usulan(){
        return $this->hasMany('App\Kategori', 'id_kategori');
    }
    public function profiledesa(){
        return $this->belongsTo('App\ProfileDesa', 'id_profiledesa');
    }
}
