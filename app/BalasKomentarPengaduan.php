<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BalasKomentarPengaduan extends Model
{
    protected $table = 'balaskomentarpengaduan';

    protected $fillable = [
        'id_komentarpengaduan',
        'tanggal',
        'nama',
        'email',
        'komentar',  	        
    	'created_at',
        'updated_at'
    ];

 	public function komentarpengaduan(){
        return $this->belongsTo('App\KomentarPengaduan', 'id_komentarpengaduan');
    }
}
