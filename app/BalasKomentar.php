<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BalasKomentar extends Model
{
    protected $table = 'balaskomentar';

    protected $fillable = [
        'id_komentarpost',
        'tanggal',
        'nama',
        'email',
        'komentar',  	        
    	'created_at',
        'updated_at'
    ];

 	public function komentarpost(){
        return $this->belongsTo('App\KomentarPost', 'id_komentarpost');
    }
}
