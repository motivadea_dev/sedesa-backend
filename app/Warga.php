<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Warga extends Model
{
	protected $table = 'warga';

    protected $fillable = [
    	'noktp',
    	'nama',
    	'alamat',
        'tanggal_lahir',
        'jenis_kelamin',
        'namapendidikan',
        'namapekerjaan',
        'namapenghasilan',
        'foto',
        'id_dusun',
        'id_profiledesa',
        'created_at',
        'updated_at'
    ];

    //Relasi One to Many dari 
    public function datauser(){
        return $this->hasOne('App\DataUser', 'id_warga');
    }
    public function usulan(){
        return $this->hasMany('App\Usulan', 'id_warga');
    }
    public function toko(){
        return $this->hasMany('App\Toko', 'id_warga');
    }
    public function transaksipenjualan(){
        return $this->hasMany('App\TransaksiPenjualan', 'id_warga');
    }
    public function pesan(){
        return $this->hasMany('App\Pesan', 'id_warga');
    }
    public function notifikasiwarga(){
        return $this->hasMany('App\NotifikasiWarga', 'id_warga');
    }
    public function dompet(){
        return $this->hasMany('App\Dompet', 'id_warga');
    }
    public function kurir(){
        return $this->hasMany('App\Kurir', 'id_warga');
    }
    public function pengaduan(){
        return $this->hasMany('App\Pengaduan', 'id_warga');
    }
    public function antrian(){
        return $this->hasMany('App\Antrian', 'id_warga');
    }
    public function perangkat(){
        return $this->hasMany('App\Perangkat', 'id_warga');
    }

    //Relasi Many to Many ke
    public function produktoko(){
        return $this->belongsToMany('App\ProdukToko', 'favorit', 'id_warga', 'id_produktoko');
    }

    //Relasi One to Many ke 
    public function dusun(){
        return $this->belongsTo('App\Dusun', 'id_dusun');
    }
    public function profiledesa(){
        return $this->belongsTo('App\ProfileDesa', 'id_profiledesa');
    }

}
