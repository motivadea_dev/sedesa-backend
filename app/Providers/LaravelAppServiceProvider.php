<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Request;

class LaravelAppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //MASTER
        $halaman = '';
        if(Request::segment(1) == 'user'){
            $halaman = 'user';
        }
        if(Request::segment(1) == 'dusun'){
            $halaman = 'dusun';
        }
        if(Request::segment(1) == 'warga'){
            $halaman = 'warga';
        }
        if(Request::segment(1) == 'kategori'){
            $halaman = 'kategori';
        }
        if(Request::segment(1) == 'usulan'){
            $halaman = 'usulan';
        }
        if(Request::segment(1) == 'hasil'){
            $halaman = 'hasil';
        }

        //ECOMMERCE
        if(Request::segment(1) == 'kategoriproduk'){
            $halaman = 'kategoriproduk';
        }
        if(Request::segment(1) == 'subkategoriproduk'){
            $halaman = 'subkategoriproduk';
        }
        if(Request::segment(1) == 'distributor'){
            $halaman = 'distributor';
        }
        if(Request::segment(1) == 'produkbumdes'){
            $halaman = 'produkbumdes';
        }
        if(Request::segment(1) == 'transaksipenjualanbumdes'){
            $halaman = 'transaksipenjualanbumdes';
        }
        if(Request::segment(1) == 'toko'){
            $halaman = 'toko';
        }
        if(Request::segment(1) == 'produktoko'){
            $halaman = 'produktoko';
        }
        if(Request::segment(1) == 'transaksipenjualan'){
            $halaman = 'transaksipenjualan';
        }
        if(Request::segment(1) == 'kurir'){
            $halaman = 'kurir';
        }
        if(Request::segment(1) == 'pengiriman'){
            $halaman = 'pengiriman';
        }
        if(Request::segment(1) == 'pesan'){
            $halaman = 'pesan';
        }
        if(Request::segment(1) == 'dompet'){
            $halaman = 'dompet';
        }
        view()->share('halaman', $halaman);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
