<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Posting extends Model
{
    protected $table = 'posting';

    protected $fillable = [
        'id_kategoripost',
    	'id_users',
        'tanggal',
        'judul',
        'judulseo',
        'isipost',
        'foto',
        'tag',
        'status',
        'headline',
        'view',
        'created_at',
        'updated_at'
    ];

    public function kategoripost(){
        return $this->belongsTo('App\KategoriPost', 'id_kategoripost');
    }
    public function user(){
        return $this->belongsTo('App\User', 'id_users');
    }
    public function komentarpost(){
        return $this->hasMany('App\KomentarPost', 'id_posting');
    }
}
