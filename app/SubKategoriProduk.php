<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubKategoriProduk extends Model
{
    protected $table = 'subkategoriproduk';

    protected $fillable = [
        'id_kategoriproduk',
    	'namasubkategori'
    ];

    //Relasi One to Many dari
    public function produktoko(){
        return $this->hasMany('App\ProdukToko', 'id_subkategoriproduk');
    }
    public function produkbumdes(){
        return $this->hasMany('App\ProdukBumdes', 'id_subkategoriproduk');
    }

    //Relasi One to Many ke
    public function kategoriproduk(){
        return $this->belongsTo('App\KategoriProduk', 'id_kategoriproduk');
    }
}
