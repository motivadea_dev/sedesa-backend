<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NotifikasiWarga extends Model
{
    protected $table = 'notifikasiwarga';
    
    protected $fillable = [
        'id_warga',
        'isi_notifikasi',
        'created_at',
        'updated_at'
    ];

    //Relasi One to Many ke
    public function warga(){
        return $this->belongsTo('App\Warga', 'id_warga');
    }
}
