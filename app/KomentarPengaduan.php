<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KomentarPengaduan extends Model
{
    protected $table = 'komentarpengaduan';

    protected $fillable = [
        'id_pengaduan',
        'tanggal',
        'nama',
        'email',
        'komentar',  	        
    	'created_at',
        'updated_at'
    ];

 	public function pengaduan(){
        return $this->belongsTo('App\Pengaduan', 'id_pengaduan');
    }
    public function balaskomentarpengaduan(){
        return $this->hasMany('App\BalasKomentarPengaduan', 'id_komentarpengaduan');
    }
}
