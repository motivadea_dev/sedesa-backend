<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ProdukTokoRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->method() == 'PATCH'){
        $kodeproduktoko_rules = 'required|string|max:50|unique:produktoko,kodeproduk,' . $this->get('id');
        }
        else{
        $kodeproduktoko_rules = 'required|string|max:50|unique:produktoko,kodeproduk';
        }
        
        return [
            'id_kategoriproduk'=> 'required',
            'id_subkategoriproduk'=> 'required',
            'id_toko'=> 'required',
            'kodeproduk' => $kodeproduktoko_rules,
            'namaproduk' => 'required',
            'stok' => 'required',
            'harga' => 'required',
            'diskon' => 'required',
            'foto' => 'sometimes|image|max:1024|mimes:jpeg,jpg,bmp,png',     
        ];
    }
}
