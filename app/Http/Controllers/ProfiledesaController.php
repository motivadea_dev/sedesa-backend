<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\ProfileDesa;

class ProfiledesaController extends Controller
{
    public function indexapp(){
        //1. Menampilkan semua data usulan dan dibagi per halaman 20 baris
        $daftarprofiledesa = Profiledesa::all();
        //2. Menghitung total keseluruhan jumlah usulan
        $jumlahprofiledesa = Profiledesa::count();
        //3. MEMBUAT DATA API / JSON UNTUK FRONT END MOBILE APPS (IONIC)
        $koleksi = collect($daftarprofiledesa);
        $koleksi->toJson();
        return $koleksi;
    }
    
    public function storeapp(Request $request)
    {
        //1. Mengambil value dari input text
        $input = $request->all();
        //2. Simpan Data usulan 
        $profiledesa = Profiledesa::create($input);
        return $usulan;
    }
    
    public function updateapp(Request $request, $item)
    {
        //1.Pencarian berdasarkan Id usulan
        $profiledesa = Profiledesa::findOrFail($item);
        //2.Mengambil data dari field edit
        $input = $request->all();
        //3.Menyimpan data usulan
        $profiledesa->update($input);
        return $profiledesa;
    }
    public function destroyapp($item)
    {
        //1. Pencarian berdasarkan Id usulan
        $profiledesa = Profiledesa::findOrFail($item);
        //2. Hapus data
        $profiledesa->delete();
        return $profiledesa;
    }
}
