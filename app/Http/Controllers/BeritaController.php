<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Dusun;
use App\Warga;
use App\Usulan;

class BeritaController extends Controller
{
    public function indexapp($item){
        settype($item, "integer");
        //1. Menampilkan semua data usulan dan dibagi per halaman 20 baris
        $daftardusun = Dusun::where('id_profiledesa',$item)->get();
        //2. Menghitung total keseluruhan jumlah usulan
        $jumlahdusun = Dusun::count();
        //3. MEMBUAT DATA API / JSON UNTUK FRONT END MOBILE APPS (IONIC)
        $koleksi = collect($daftardusun);
        $koleksi->toJson();
        return $koleksi;
    }
    public function detailhasil($item)
    {
        settype($item, "integer");
        //1. Menampilkan semua data warga dan dibagi per halaman 20 baris
        $daftarwarga = Warga::where('id_dusun',$item)->get();
        $koleksi = collect($daftarwarga);
        $koleksi->toJson();
       
        //$daftar2 = Usulan::all();
        $daftarusulan = Usulan::with('kategori','fotousulan')->where('status', 2)->get();
        $koleksi2 = collect($daftarusulan);
        //$koleksi2->toJson();
        //2. Menghitung total keseluruhan jumlah warga
        //$jumlahwarga = Warga::count();
        //3. MEMBUAT DATA API / JSON UNTUK 
        return compact('koleksi','koleksi2');
    }
    public function storeapp(Request $request)
    {
        //1. Mengambil value dari input text
        $input = $request->all();
        //2. Simpan Data usulan 
        $berita = Berita::create($input);
        return $berita;
    }
    
    public function updateapp(Request $request, $item)
    {
        //1.Pencarian berdasarkan Id usulan
        $berita = Berita::findOrFail($item);
        //2.Mengambil data dari field edit
        $input = $request->all();
        //3.Menyimpan data usulan
        $berita->update($input);
        return $berita;
    }
    public function destroyapp($item)
    {
        //1. Pencarian berdasarkan Id usulan
        $berita = Berita::findOrFail($item);
        //2. Hapus data
        $berita->delete();
        return $berita;
    }
}
