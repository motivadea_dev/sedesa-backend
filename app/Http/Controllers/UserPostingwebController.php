<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\PostingRequest;
use App\Http\Controllers\Controller;

use App\Posting;
use App\KomentarPost;
use App\Warga;
use App\BalasKomentar;
use App\KategoriPost;
use Auth;
use Storage;
use Session;

class UserPostingwebController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $iduser = Auth::id();
        settype($iduser, "integer");
        //1. Menampilkan semua data warga dan dibagi per halaman 20 baris
        $daftarposting = Posting::where('id_users', $iduser)->orderBy('tanggal', 'desc')->paginate(20);;
        //2. Menghitung total keseluruhan jumlah warga
        $jumlahposting = Posting::count();
        //3. MEMBUAT DATA API / JSON UNTUK 
        return view('userposting.index', compact('daftarposting','jumlahposting'));
    }

    public function indexkomentar(Posting $posting)
    {
           $daftar = KomentarPost::with('balaskomentar')->where('id_posting',$posting->id)->get();
           $jumlahkomentar = $daftar->count();
           $daftarkomentar = collect($daftar);
            $daftarkomentar->toJson();
           return view('userposting.komentar', compact('daftarkomentar','jumlahkomentar'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('userposting.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostingRequest $request)
    {
        $input = $request->all();
        if($request->hasFile('foto')){
            $input['foto'] = $this->uploadFoto($request);
        }
        else{
            $input['foto'] = "nophoto.jpg";
        }
        $posting = Posting::create($input);
        Session::flash('flash_message', 'Posting Berhasil Disimpan');
        return redirect('userposting');
    }
    public function balasan(Request $request)
    {
        $input = $request->all();
        $balasan = BalasKomentar::create($input);
        Session::flash('flash_message', 'Balasan Berhasil Disimpan');
        return redirect('userposting');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Posting $posting)
    {
        return view('userposting.show',compact('posting'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Posting $posting)
    {
        return view('userposting.edit', compact('posting'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PostingRequest $request,Posting $posting)
    {
        $input = $request->all();
        if($request->hasFile('foto')){
            //Hapus foto lama
            $this->hapusFoto($posting);
            //Upload foto baru
            $input['foto'] = $this->uploadFoto($request);
        }
        $posting->update($input);

        Session::flash('flash_message', 'Posting berhasil diupdate');
        return redirect('userposting');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Posting $posting)
    {
        $posting->delete();
        Session::flash('flash_message', 'Posting berhasil dihapus');
        Session::flash('Penting', true);
        return redirect('userposting');
    }
    public function destroykomentar($komentar)
    {
        $daftarkomentar = KomentarPost::findOrFail($komentar);
        $daftarkomentar->delete();
        Session::flash('flash_message', 'Komentar berhasil dihapus');
        Session::flash('Penting', true);
        return redirect('userposting');
    }
    public function destroybalasan($balasan)
    {
        $daftarbalasan = BalasKomentar::findOrFail($balasan);
        $daftarbalasan->delete();
        Session::flash('flash_message', 'Balasan berhasil dihapus');
        Session::flash('Penting', true);
        return redirect('userposting');
    }

    //Upload foto ke direktori lokal
    public function uploadFoto(PostingRequest $request){
        $foto = $request->file('foto');             //Mengambil Inputan file foto
        $ext = $foto->getClientOriginalExtension(); //Extension (.jpg, .png ,dll)

        if($request->file('foto')->isValid()){      //Kondisi jika foto valid (nek format e selain jpg/png gak iso)    
            $foto_name = date('YmdHis'). ".$ext";   //Penamaan File (berdasarkan tanggal/jam) dadi iso bedo2 orak ketumpuk
            $upload_path = 'fotoupload';            //Folder foto sing diupload ke server
            $request->file('foto')->move($upload_path, $foto_name); //Copy paste ke server
            return $foto_name;
        }
        return false;
    }

    //Hapus foto di direktori lokal
    public function hapusFoto(Posting $posting){
        $exist = Storage::disk('foto')->exists($posting->foto);
        if(isset($posting->foto) && $exist){
           $delete = Storage::disk('foto')->delete($posting->foto);
           if($delete){
            return true;
           }
           return false;
        }
    }
}
