<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('username')->unique();
            $table->string('password');
            $table->rememberToken();
            $table->enum('level', ['admin','dusun','desa','warga','pelaksana','bumdes']);
            $table->enum('status', ['1','2']);
            $table->string('fotoktp')->nullable;
            $table->string('fotowajah')->nullable;
            $table->string('nohp');
            $table->string('email');
            $table->integer('id_dusun')->unsigned();
            $table->timestamps();
        });

        Schema::table('posting', function(Blueprint $table) {
            $table->foreign('id_users')
                ->references('id')
                ->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('posting', function(Blueprint $table) {
            $table->dropForeign('posting_id_users_foreign');
        });
        Schema::drop('users');
    }
}
