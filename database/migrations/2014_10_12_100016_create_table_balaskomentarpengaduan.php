<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableBalaskomentarpengaduan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('balaskomentarpengaduan', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_komentarpengaduan')->unsigned();
            $table->date('tanggal');
            $table->string('nama');
            $table->string('email');
            $table->text('komentar');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('balaskomentarpengaduan');
    }
}
