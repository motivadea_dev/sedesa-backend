<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableToko extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('toko', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_warga')->unsigned();
            $table->string('kodetoko', 50)->unique();
            $table->string('namatoko', 50);
            $table->text('alamat');
            $table->string('foto')->nullable;
            $table->enum('status',['buka','tutup']);
            $table->timestamps();
        });

        Schema::table('produktoko', function(Blueprint $table) {
            $table->foreign('id_toko')
                ->references('id')
                ->on('toko')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

        Schema::table('transaksipenjualan', function(Blueprint $table) {
            $table->foreign('id_toko')
                ->references('id')
                ->on('toko')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

        Schema::table('transaksipembelian', function(Blueprint $table) {
            $table->foreign('id_toko')
                ->references('id')
                ->on('toko')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

        Schema::table('notifikasitoko', function(Blueprint $table) {
            $table->foreign('id_toko')
                ->references('id')
                ->on('toko')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

        Schema::table('pesan', function(Blueprint $table) {
            $table->foreign('id_toko')
                ->references('id')
                ->on('toko')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
        Schema::table('perangkattoko', function(Blueprint $table) {
            $table->foreign('id_toko')
                ->references('id')
                ->on('toko')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('produktoko', function(Blueprint $table) {
            $table->dropForeign('produktoko_id_toko_foreign');
        });
        Schema::table('transaksipenjualan', function(Blueprint $table) {
            $table->dropForeign('transaksipenjualan_id_toko_foreign');
        });
        Schema::table('transaksipembelian', function(Blueprint $table) {
            $table->dropForeign('transaksipembelian_id_toko_foreign');
        });
        Schema::table('notifikasitoko', function(Blueprint $table) {
            $table->dropForeign('notifikasitoko_id_toko_foreign');
        });
        Schema::table('pesan', function(Blueprint $table) {
            $table->dropForeign('pesan_id_toko_foreign');
        });
        Schema::table('perangkattoko', function(Blueprint $table) {
            $table->dropForeign('perangkattoko_id_toko_foreign');
        });
        Schema::drop('toko');
    }
}
