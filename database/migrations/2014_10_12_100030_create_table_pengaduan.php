<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePengaduan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pengaduan', function (Blueprint $table) {
            $table->increments('id');
            $table->date('tanggal');
            $table->string('judul');
            $table->integer('id_kategoripengaduan')->unsigned();
            $table->integer('id_dusun')->unsigned();
            $table->integer('id_warga')->unsigned();
            $table->string('longitude');
            $table->string('latitude');
            $table->text('deskripsi');
            $table->enum('privasipengaduan',['1','2']);
            $table->enum('privasiidentitas',['1','2']);
            $table->enum('status',['1','2','3']);
            $table->timestamps();
        });
        Schema::table('fotopengaduan', function(Blueprint $table) {
            $table->foreign('id_pengaduan')
                ->references('id')
                ->on('pengaduan')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
        Schema::table('komentarpengaduan', function(Blueprint $table) {
            $table->foreign('id_pengaduan')
                ->references('id')
                ->on('pengaduan')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
        Schema::table('tindakanpengaduan', function(Blueprint $table) {
            $table->foreign('id_pengaduan')
                ->references('id')
                ->on('pengaduan')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fotoupengaduan', function(Blueprint $table) {
            $table->dropForeign('fotoupengaduan_id_pengaduan_foreign');
        });
        Schema::table('komentarpengaduan', function(Blueprint $table) {
            $table->dropForeign('komentarpengaduan_id_pengaduan_foreign');
        });
        Schema::table('tindakanpengaduan', function(Blueprint $table) {
            $table->dropForeign('tindakanpengaduan_id_pengaduan_foreign');
        });
        Schema::drop('pengaduan');
    }
}
