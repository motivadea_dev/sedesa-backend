<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableUsulan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usulan', function (Blueprint $table) {
            $table->increments('id');
            $table->date('tanggal');
            $table->string('judul');
            $table->integer('id_kategori')->unsigned();
            $table->integer('id_warga')->unsigned();
            $table->string('lokasi');
            $table->decimal('longitude', 11, 8);
            $table->decimal('latitude', 11, 8);
            $table->string('volume');
            $table->string('satuan');
            $table->string('pria');
            $table->string('wanita');
            $table->string('rtm');
            $table->text('deskripsi');
            $table->enum('status',['1','2','3']);
            $table->enum('prioritas',['1','2']);
            $table->integer('rangking');
            $table->timestamps();
        });

        Schema::table('fotousulan', function(Blueprint $table) {
            $table->foreign('id_usulan')
                ->references('id')
                ->on('usulan')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
        Schema::table('proposalusulan', function(Blueprint $table) {
            $table->foreign('id_usulan')
                ->references('id')
                ->on('usulan')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
        Schema::table('polling', function(Blueprint $table) {
            $table->foreign('id_usulan')
                ->references('id')
                ->on('usulan')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fotousulan', function(Blueprint $table) {
            $table->dropForeign('fotousulan_id_usulan_foreign');
        });
        Schema::table('proposalusulan', function(Blueprint $table) {
            $table->dropForeign('proposalusulan_id_usulan_foreign');
        });
        Schema::table('polling', function(Blueprint $table) {
            $table->dropForeign('polling_id_usulan_foreign');
        });
        Schema::drop('usulan');
    }
}
