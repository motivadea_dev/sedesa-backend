<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableProdukbumdes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('produkbumdes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_kategoriproduk')->unsigned();
            $table->integer('id_subkategoriproduk')->unsigned();
            $table->integer('id_distributor')->unsigned();
            $table->string('kodeproduk', 50)->unique();
            $table->string('namaproduk', 50);
            $table->integer('stok');
            $table->double('harga');
            $table->double('diskon');
            $table->string('foto')->nullable;
            $table->timestamps();
        });
        Schema::table('detailpembelian', function(Blueprint $table) {
            $table->foreign('id_produkbumdes')
                ->references('id')
                ->on('produkbumdes')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
        Schema::table('keranjangtoko', function(Blueprint $table) {
            $table->foreign('id_produkbumdes')
                ->references('id')
                ->on('produkbumdes')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('detailpembelian', function(Blueprint $table) {
            $table->dropForeign('detailpembelian_id_produkbumdes_foreign');
        });
        Schema::table('keranjangtoko', function(Blueprint $table) {
            $table->dropForeign('keranjangtoko_id_produkbumdes_foreign');
        });
        Schema::drop('produkbumdes');
    }
}
