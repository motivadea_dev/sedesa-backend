<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableKomentarpengaduan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('komentarpengaduan', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_pengaduan')->unsigned();
            $table->date('tanggal');
            $table->string('nama');
            $table->string('email');
            $table->text('komentar');
            $table->timestamps();
        });
        Schema::table('balaskomentarpengaduan', function(Blueprint $table) {
            $table->foreign('id_komentarpengaduan')
                ->references('id')
                ->on('komentarpengaduan')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('balaskomentarpengaduan', function(Blueprint $table) {
            $table->dropForeign('balaskomentarpengaduan_id_komentarpengaduan_foreign');
        });
        Schema::drop('komentarpengaduan');
    }
}
