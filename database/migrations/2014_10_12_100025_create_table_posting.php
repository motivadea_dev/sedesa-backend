<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePosting extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posting', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_kategoripost')->unsigned();
            $table->integer('id_users')->unsigned();
            $table->date('tanggal');
            $table->string('judul');
            $table->string('judulseo');
            $table->text('isipost');
            $table->string('foto')->nullable;
            $table->text('tag');
            $table->enum('status',['1','2']);
            $table->enum('headline',['1','2']);
            $table->integer('view');
            $table->timestamps();
        });
        Schema::table('komentarpost', function(Blueprint $table) {
            $table->foreign('id_posting')
                ->references('id')
                ->on('posting')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('komentarpost', function(Blueprint $table) {
            $table->dropForeign('komentarpost_id_posting_foreign');
        });

        Schema::drop('posting');
    }
}
